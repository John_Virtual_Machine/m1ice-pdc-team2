package dsl.function;

import api.entity.Movie;
import org.json.JSONArray;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class EconomicRatioTest extends DslFunctionTest {

    @Override
    protected DslFunction getDslFunctionToTest() {
        ArrayList<Movie> movies = new ArrayList<>();
        movies.add(new Movie(1726));
        movies.add(new Movie(10138));

        return new EconomicRatio(movies);
    }

    @Test
    public void hasCorrectStructure() {

        System.out.println(result);
        assertTrue(result.getInt("id") > -1);
        assertEquals("Economic Ratio", result.getString("name"));
        assertEquals("Economic Ratio", result.getString("subtitle"));
        assertTrue(result.has("data"));
        assertTrue(result.getJSONObject("data").has("labels"));
        assertTrue(result.getJSONObject("data").get("labels") instanceof JSONArray);
        assertTrue(result.has("datasets"));
        assertTrue(result.getJSONArray("datasets").getJSONObject(0).has("data"));
        assertTrue(result.getJSONArray("datasets").getJSONObject(0).get("data") instanceof JSONArray);
    }
}