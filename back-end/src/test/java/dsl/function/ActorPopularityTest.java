package dsl.function;

import org.json.JSONArray;
import org.junit.Test;

import static org.junit.Assert.*;

public class ActorPopularityTest extends DslFunctionTest {

    @Override
    protected DslFunction getDslFunctionToTest() {
        return new ActorPopularity("Bruce Willis");
    }

    @Test
    public void hasCorrectStructure() {
        assertTrue(result.getInt("id") > -1);
        assertEquals("Actors popularity", result.getString("name"));
        assertEquals("Bruce Willis", result.getString("subtitle"));
        assertTrue(result.has("data"));
        assertTrue(result.getJSONObject("data").has("labels"));
        assertTrue(result.getJSONObject("data").get("labels") instanceof JSONArray);
        assertTrue(result.has("datasets"));
        assertTrue(result.getJSONArray("datasets").getJSONObject(0).has("data"));
        assertTrue(result.getJSONArray("datasets").getJSONObject(0).get("data") instanceof JSONArray);
    }
}
