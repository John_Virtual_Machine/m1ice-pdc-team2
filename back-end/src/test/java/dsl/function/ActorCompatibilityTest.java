package dsl.function;

import org.json.JSONArray;
import org.junit.Test;

import static org.junit.Assert.*;

public class ActorCompatibilityTest extends DslFunctionTest {

    @Override
    protected DslFunction getDslFunctionToTest() {
        return new ActorCompatibility("Brad Pitt", "Angelina Jolie");
    }

    @Test
    public void hasCorrectStructure() {
        assertTrue(result.getInt("id") > -1);
        assertEquals("Actor Compatibility", result.getString("name"));
        assertEquals("Brad Pitt, Angelina Jolie", result.getString("subtitle"));
        assertTrue(result.has("data"));
        assertTrue(result.getJSONObject("data").has("labels"));
        assertTrue(result.getJSONObject("data").get("labels") instanceof JSONArray);
        assertTrue(result.has("datasets"));
        assertTrue(result.getJSONArray("datasets").getJSONObject(0).has("data"));
        assertTrue(result.getJSONArray("datasets").getJSONObject(0).get("data") instanceof JSONArray);
    }
}