package dsl.function;

import org.json.JSONObject;
import org.junit.Before;

public abstract class DslFunctionTest {

    JSONObject result;

    @Before
    public void setUp() {
        this.result = this.getDslFunctionToTest().execute();
    }

    protected abstract DslFunction getDslFunctionToTest();

}