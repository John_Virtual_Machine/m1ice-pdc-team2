package api.entity;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class ActorTest {

    Actor bruce;

    @Before
    public void setUp() throws InterruptedException {
        this.bruce = new Actor("Bruce Willis");
        Thread.sleep(6000);
    }

    @After
    public void tearDown() throws InterruptedException {
        Thread.sleep(6000);
    }

    @Test
    public void testGetTenLastMovies() {

        ArrayList<Movie> movies = bruce.getTenLastMovies();

        assertEquals(10, movies.size());

        assertEquals(395990, movies.get(0).getId());
        assertEquals(479040, movies.get(1).getId());
        assertEquals(410554, movies.get(2).getId());
        assertEquals(345915, movies.get(3).getId());
        assertEquals(381288, movies.get(4).getId());
        assertEquals(486068, movies.get(5).getId());
        assertEquals(384737, movies.get(6).getId());
        assertEquals(359412, movies.get(7).getId());
        assertEquals(387415, movies.get(8).getId());
        assertEquals(326425, movies.get(9).getId());
    }

    @Test
    public void testGetPopularity() {
        assertNotNull(bruce.getPopularity());
    }

    @Test
    public void testGetName() {
        assertEquals("Bruce Willis", bruce.getName());
    }
}