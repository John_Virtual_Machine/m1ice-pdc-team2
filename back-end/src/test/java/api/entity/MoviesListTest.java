package api.entity;

import api.entity.collection.PopularMovieCollection;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MoviesListTest {

    @Test
    public void testGetMoviesList() { assertEquals(20, PopularMovieCollection.getAll().size()); }

}
