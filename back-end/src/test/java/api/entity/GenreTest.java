package api.entity;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class GenreTest {

    Genre genre;

    @Before
    public void setUp() {
        this.genre = new Genre(14, "Fantasy");
    }

    @After
    public void tearDown() throws InterruptedException {
        Thread.sleep(6000);
    }

    @Test
    public void testGetName() {
        assertEquals("Fantasy", genre.getName());
    }


}
