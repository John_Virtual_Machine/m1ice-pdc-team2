package api.entity;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.*;

public class MovieTest {

    Movie movie;

    @Before
    public void setUp() {
        this.movie = new Movie(345934);
    }

    @After
    public void tearDown() throws InterruptedException {
        Thread.sleep(6000);
    }

    @Test
    public void testConstructorNameParameter() {
        this.movie = new Movie("John Wick");

        assertEquals(245891, this.movie.getId());
        assertEquals("John Wick", this.movie.getTitle());
        assertEquals("Action", this.movie.getGenres().get(0).getName());
        assertEquals("Thriller", this.movie.getGenres().get(1).getName());
    }

    @Test
    public void testConstructorResponseParameter() {
        JSONObject response = new JSONObject("{\"overview\":\"Depuis la mort de sa femme bien-aimée, John Wick passe ses journées à retaper sa Ford Mustang de 1969, avec pour seule compagnie sa chienne Daisy. Il mène une vie sans histoire, jusqu\\u2019à ce qu\\u2019un malfrat sadique nommé Iosef Tarasof remarque sa voiture. John refuse de la lui vendre. Iosef n\\u2019acceptant pas qu\\u2019on lui résiste, s\\u2019introduit chez John avec deux complices pour voler la Mustang, et tuer sauvagement Daisy\\u2026  John remonte la piste de Iosef jusqu\\u2019à New York. Un ancien contact, Aurelio, lui apprend que le malfrat est le fils unique d\\u2019un grand patron de la pègre, Viggo Tarasof. La rumeur se répand rapidement dans le milieu : le légendaire tueur cherche Iosef. Viggo met à prix la tête de John : quiconque l\\u2019abattra touchera une énorme récompense. John a désormais tous les assassins de New York aux trousses.\",\"original_language\":\"en\",\"original_title\":\"John Wick\",\"video\":false,\"title\":\"John Wick\",\"genre_ids\":[28,53],\"poster_path\":\"/tyPtD94Gx1JFkWgGO3nrmv1GmpL.jpg\",\"backdrop_path\":\"/umC04Cozevu8nn3JTDJ1pc7PVTn.jpg\",\"release_date\":\"2014-10-22\",\"vote_average\":7,\"popularity\":82.180896,\"id\":245891,\"adult\":false,\"vote_count\":7207}");
        this.movie = new Movie(response, false);

        assertEquals(245891, this.movie.getId());
        assertEquals("John Wick", this.movie.getTitle());
        assertEquals("Action", this.movie.getGenres().get(0).getName());
        assertEquals("Thriller", this.movie.getGenres().get(1).getName());
    }

    @Test
    public void testGetReleaseDateWithExistingDate() throws ParseException {

        this.movie = new Movie(395990);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date expectedDate = simpleDateFormat.parse("2018-03-02");

        assertEquals(expectedDate, movie.getReleaseDate());
    }

    @Test
    public void testGetReleaseDateWithEmptyDate() throws ParseException {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date longTimeAgo = simpleDateFormat.parse("1900-01-01");

        assertTrue(movie.getReleaseDate().before(longTimeAgo));
    }

    @Test
    public void testGetPopularity() {

        assertNotNull(movie.getPopularity());
    }

    @Test
    public void testGetTitle() {

        assertEquals("The Bombing", movie.getTitle());
    }

    @Test
    public void testGetGenres() {
        assertNotNull(movie.getGenres());
    }

    @Test
    public void testGetRevenue() {

        this.movie = new Movie(69);
        assertEquals(186438883, movie.getRevenue());
    }

    @Test
    public void testGetBudget() {

        this.movie = new Movie(69);
        assertEquals(28000000, movie.getBudget());
    }

    @Test
    public void testGetEconomicRatio() {

        this.movie = new Movie(69);
        assertEquals(6.659999847412109, movie.getEconomicRatio(), 0.01);
    }

    @Test
    public void testEqualsIsEqual() {

        Movie otherMovie = new Movie(345934);
        assertTrue(this.movie.equals(otherMovie));
    }

    @Test
    public void testEqualsIsNotEqual() {

        Movie otherMovie = new Movie(62);
        assertFalse(this.movie.equals(otherMovie));
    }


}
