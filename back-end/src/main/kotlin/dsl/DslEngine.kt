package dsl

import api.entity.Movie
import dsl.function.*
import mps.output.dsl.generated.DslTranslator
import org.json.JSONArray
import org.json.JSONObject
import java.util.ArrayList

/**
 * This singleton class contains is used by the MainController to get all data
 * that will be exploited by the front-end.
 */
object DslEngine {

    private var translator = DslTranslator()

    /**
     * Get all data asked by the user in the DSL.
     * @return data : data asked by the user.
     */
    @JvmStatic
    fun getData(): String {
        val data = mutableListOf<JSONObject>()

        data.addAll(this.getActorPopularityData())
        data.addAll(this.getEconomicRatioData())
        data.addAll(this.getActorCompatibilityData())
        data.add(this.getMovieGenreTrendData())

        DslFunction.DATA_ID = -1

        return JSONArray(data).toString()
    }


    /**
     * Get all data asked by the user relative to the "actor popularity"
     * DSL method.
     * @return actorPopularityData : list of json object with actor popularity data.
     */
    private fun getActorPopularityData(): ArrayList<JSONObject> {

        val desiredActorPopularity = translator.desiredActorPopularity
        val actorPopularityData = ArrayList<JSONObject>()

        desiredActorPopularity.forEach { actorName ->
            val actorPopularity = ActorPopularity(actorName)
            actorPopularityData.add(actorPopularity.execute())
        }
        return actorPopularityData
    }

    /**
     * Get all data asked by the user relative to the "economic ratio"
     * DSL method.
     * @return economicRatioData : list of json object with economic ratio data.
     */
    private fun getEconomicRatioData(): ArrayList<JSONObject> {

        val desiredEconomicRatios = translator.desiredEconomicRatio
        val economicRatioData = ArrayList<JSONObject>()

        desiredEconomicRatios.forEach { economicRatioParam ->
            val movies = ArrayList<Movie>()

            for (i in 0..4) {

                if (economicRatioParam[i] !== "null")
                    movies.add(Movie(economicRatioParam[i]))
            }
            val economicRatio = EconomicRatio(movies)
            economicRatioData.add(economicRatio.execute())
        }
        return economicRatioData
    }

    /**
    * Get all data asked by the user relative to the "actor compatibility"
    * DSL method.
    * @return actorCompatibilityData : list of json object with actor compatibility data.
    */
    private fun getActorCompatibilityData(): ArrayList<JSONObject> {

        val desiredActorCompatibilities = translator.desiredActorCompatibility
        val actorCompatibilityData = ArrayList<JSONObject>()

        desiredActorCompatibilities.forEach { actorCompatibilityParam ->

            val actorCompatibility = ActorCompatibility(
                    actorCompatibilityParam[0],
                    actorCompatibilityParam[1]
            )
            actorCompatibilityData.add(actorCompatibility.execute())
        }
        return actorCompatibilityData
    }

    /**
     * Get data asked by the user relative to the "movie genre trend" DSL method.
     * @return movieGenreTrendData : son object with movie genre trend data.
     */
    private fun getMovieGenreTrendData(): JSONObject =  MoviesGenreTrend().execute()
}