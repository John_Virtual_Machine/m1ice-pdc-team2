package api.request

import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.web.client.RestTemplate

/**
 * This trait is used by every entities that need to interact with the API.
 */
interface ApiRequester {

    /**
     * Request the link specified in the "uri" param. There's a limited number of
     * chained request. To avoid 429 when this number is reach, you have to wait
     * until a specific timestamp, provided by the API.
     * @param uri : the uri to request.
     * @return resultString : the uri response.
     */
    fun requestApi(uri: String): String {

        if (TIMESTAMP != 0 && TIMESTAMP >= System.currentTimeMillis() / 1000 && NB_REQUEST_REMAINING < 2) {
            while (TIMESTAMP >= System.currentTimeMillis() / 1000) {
                try {
                    Thread.sleep(500)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }

            }
        }
        val template = RestTemplate()
        var headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON
        val request = HttpEntity(String(), headers)
        val response = template.exchange(uri, HttpMethod.GET, request, String::class.java)

        val resultString = response.body

        headers = response.headers
        TIMESTAMP = Integer.parseInt(headers["X-RateLimit-Reset"]!![0])
        NB_REQUEST_REMAINING = Integer.parseInt(headers["X-RateLimit-Remaining"]!![0])

        return resultString
    }
}