package api.request

/**
 * Our key of the themoviedb API.
 */
const val API_KEY ="4f6dfbb5cf6055c9b9ca9b93f9f519a8"

/**
 * The timestamp date of the moment where the API can be requested again.
 * There's a limited number of chained request. To avoid 429 when this number
 * is reach, you have to wait until a specific timestamp, provided by the API.
 */
var TIMESTAMP = 0

/**
 * The number of remaining request before we have to wait.
 * There's a limited number of chained request. To avoid 429 when this number
 * is reach, you have to wait until a specific timestamp, provided by the API.
 */
var NB_REQUEST_REMAINING = 40
