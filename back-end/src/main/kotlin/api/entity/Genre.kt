package api.entity

import org.json.JSONObject

/**
 * The Genre class index all the useful data about a specifics genre.
 */
class Genre(override val id: Int, val name: String): ApiEntity() {

    /**
     * Construct a Genre object based on the genreJsonObject provided by
     * the API database.
     * @param genreJsonObject : the json object genre details.
     */
    constructor(genreJsonObject: JSONObject): this(genreJsonObject.getInt("id"), genreJsonObject.getString("name"))

    /**
     * Returns a string representation of the object.
     */
    override fun toString(): String {
        return this.id.toString() + "_" + this.name
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     */
    override fun equals(other: Any?): Boolean {
        return if (other != null && other is Genre) this.id == other.id else false
    }
}