package api.entity.collection

import api.entity.Genre
import api.request.API_KEY
import api.request.ApiRequester
import org.json.JSONArray
import org.json.JSONObject

/**
 * This singleton class contains all movie genre available in the
 * themoviedb API.
 */
object GenreCollection: ApiRequester {

    private val genres =  mutableSetOf<Genre>()

    init {
        val allGenreJsonArray = this.requestAllGenre()

        for (i in 0 until allGenreJsonArray.length()) {
            val result = allGenreJsonArray.getJSONObject(i)
            val genre = Genre(result)
            this.addInCollection(genre)
        }
    }

    /**
     * Find in the list the genre identified by the id param.
     * @param id : genre id to find.
     * @return a genre if found, null otherwise.
     */
    @JvmStatic
    fun find(id: Int): Genre? = this.genres.firstOrNull { it.id == id }

    /**
     * Add a new genre to the sorted list genreList.
     * @param genre : new genre to add.
     */
    private fun addInCollection(genre: Genre) {
        this.genres.add(genre)
        this.genres.sortedBy { it.id }
    }

    /**
     * Request themoviedb API to get the list of all available
     * movie genre.
     * @return genres : json array of all available genre.
     */
    private fun requestAllGenre(): JSONArray {
        val uri = "https://api.themoviedb.org/3/genre/movie/list?api_key=$API_KEY&language=fr-FR"
        return JSONObject(this.requestApi(uri)).getJSONArray("genres")
    }
}