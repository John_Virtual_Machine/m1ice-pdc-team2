package api.entity.collection

import api.entity.Movie
import api.request.API_KEY
import api.request.ApiRequester
import org.json.JSONArray
import org.json.JSONObject

/**
 * This singleton class contains all trending popular movies. The popularity is defined by the
 * themoviedb API, based on their data.
 */
object PopularMovieCollection: ApiRequester {

    private val popularMovies =  mutableSetOf<Movie>()

    init {
        val allPopularMovieJsonArray = this.requestAllPopularMovie()

        for (i in 0 until allPopularMovieJsonArray.length()) {
            val result = allPopularMovieJsonArray.getJSONObject(i)
            val movie = Movie(result, false)
            this.popularMovies.add(movie)
        }
    }

    /**
     * Get all trending popular movies.
     * @return popularMovies : the trending popular movies.
     */
    @JvmStatic
    fun getAll(): Set<Movie> = this.popularMovies.toSet()

    /**
     * Request themoviedb API to get the list of the trending popular movies.
     * @return genres : json array of the trending popular movies.
     */
    private fun requestAllPopularMovie(): JSONArray {
        val uri = "https://api.themoviedb.org/3/movie/popular?api_key=$API_KEY&language=fr-FR&page=1"
        return JSONObject(this.requestApi(uri)).getJSONArray("results")
    }
}