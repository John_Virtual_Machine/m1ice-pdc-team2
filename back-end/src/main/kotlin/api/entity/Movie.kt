package api.entity

import api.entity.collection.GenreCollection
import api.request.API_KEY
import api.request.ApiRequester
import org.json.JSONObject
import java.math.BigDecimal
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * The Movie class index all the useful data about a specifics movie.
 */
class Movie(): ApiEntity(), ApiRequester {

    override var id: Int = 0
    lateinit var title: String
    lateinit var releaseDate: Date

    var popularity = 0.0
    var budget = 0L
    var revenue = 0L
    val genres = ArrayList<Genre>()

    /**
     * Construct a Movie object based on its id in the API database.
     * @param id : the movie id.
     */
    constructor(id: Int): this() {
        this.id = id
        this.instantiateAttributes(this.requestMovieDetails())
    }

    /**
     * Construct a Movie object based on its title.
     * @param titleParam : the movie title.
     */
    constructor(titleParam: String): this() {
        this.id = this.requestSearchByTitle(titleParam).getInt("id")
        this.instantiateAttributes(this.requestMovieDetails())
    }

    /**
     * Construct a Movie object based on the movieJsonObject provided by
     * the API database. The json object might not be enough complete, we
     * need deeper data about it to instantiate all attributes properly.
     * @param movieJsonObject : the json object movie details.
     * @param needDeeperInformation : is the program need to get deeper information ?
     */
    constructor(movieJsonObject: JSONObject, needDeeperInformation: Boolean): this() {
        this.id = movieJsonObject.getInt("id")

        if (needDeeperInformation)
            this.instantiateAttributes(this.requestMovieDetails())
        else
            instantiateAttributes(movieJsonObject)
    }

    /**
     * Get the economic ratio of the movie, dividing the budget by the budget
     * by the revenue.
     * @return economicRatio : he economic ratio of the movie.
     */
    fun getEconomicRatio(): Float {
        return if (this.budget != 0L && this.revenue != 0L) {
            val ratio = this.revenue / this.budget.toFloat()
            var bd = BigDecimal(java.lang.Float.toString(ratio))
            bd = bd.setScale(2, BigDecimal.ROUND_UP)
            bd.toFloat()
        } else 0f
    }

    /**
     * Returns a string representation of the object.
     */
    override fun toString(): String {
        return "{id: " + this.id + ", releaseDate: " + this.releaseDate.toLocaleString() + "}"
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     */
    override fun equals(other: Any?): Boolean {
        return if (other != null && other is Movie) this.id == other.id else false
    }

    /**
     * Instantiate all object attributes based on the json object values.
     * @param movieJsonObject : the json object movie details.
     */
    private fun instantiateAttributes(movieJsonObject: JSONObject) {

        this.title = movieJsonObject.getString("title")
        this.setReleaseDateByJson(movieJsonObject)
        this.popularity = movieJsonObject.getDouble("popularity")
        this.setGenresByJson(movieJsonObject)

        if (movieJsonObject.has("revenue"))
            this.revenue = movieJsonObject.getLong("revenue")
        if (movieJsonObject.has("budget"))
            this.budget = movieJsonObject.getLong("budget")
    }

    /**
     * Set the release date attributes based on the values of the json object.
     * The "release_data" key might be missing from the json object, it means the movie has
     * not be released yet. In this case, the released date is set to 1800 in order to be
     * ignored by the future logic of this program.
     * @param movieJsonObject : the json object movie details.
     */
    private fun setReleaseDateByJson(movieJsonObject: JSONObject) {
        val calendar = Calendar.getInstance()
        calendar.set(1800, Calendar.JANUARY, 1)
        var releaseDate = calendar.time

        if (movieJsonObject.has("release_date")) {
            val releaseDateString = movieJsonObject.getString("release_date")
            if (releaseDateString != "") {
                try {
                    val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")
                    releaseDate = simpleDateFormat.parse(releaseDateString)
                } catch (e: ParseException) {
                    e.printStackTrace()
                }

            }
        }
        this.releaseDate = releaseDate
    }

    /**
     * Set the genres attributes based on the values of the json object. The API
     * provides genre data in two different ways : with genres keys and with genreIds
     * keys.
     * @param movieJsonObject : the json object movie details.
     */
    private fun setGenresByJson(movieJsonObject: JSONObject) {
        if (movieJsonObject.has("genres"))
            this.setGenreWithGenresKey(movieJsonObject)
        else
            this.setGenreWithGenreIdsKey(movieJsonObject)
    }

    /**
     * Set the genres attributes based on the values of the json object with genres
     * keys.
     * @param movieJsonObject : the json object movie details.
     */
    private fun setGenreWithGenresKey(movieJsonObject: JSONObject) {
        val genresJsonArray = movieJsonObject.getJSONArray("genres")
        for (i in 0 until genresJsonArray.length())
            this.genres.add(Genre(genresJsonArray.getJSONObject(i)))
    }

    /**
     * Set the genres attributes based on the values of the json object with genreIds
     * keys.
     * @param movieJsonObject : the json object movie details.
     */
    private fun setGenreWithGenreIdsKey(movieJsonObject: JSONObject) {
        val genresIdsJsonArray = movieJsonObject.getJSONArray("genre_ids")
        for (i in 0 until genresIdsJsonArray.length()) {
            val genre = GenreCollection.find(genresIdsJsonArray.getInt(i))
            if (genre != null)
                this.genres.add(genre)
        }
    }

    /**
     * Request themoviedb API to get the movie details.
     * @return movieJsonObject : json object of the movie details.
     */
    private fun requestMovieDetails(): JSONObject {
        val uri = "https://api.themoviedb.org/3/movie/$id?api_key=$API_KEY&language=fr"
        return JSONObject(this.requestApi(uri))
    }

    /**
     * Request themoviedb API to search a movie by its name. The first result is
     * always selected.
     * @pram titleParam : the title of the movie we're looking for.
     * @return searchResult : json object of the first search result.
     */
    private fun requestSearchByTitle(titleParam: String): JSONObject {
        val uri = "https://api.themoviedb.org/3/search/movie?api_key=$API_KEY&language=fr&query=$titleParam&page=1&include_adult=false"
        val response = JSONObject(this.requestApi(uri))
        val results = response.getJSONArray("results")

        return results.getJSONObject(0)
    }
}