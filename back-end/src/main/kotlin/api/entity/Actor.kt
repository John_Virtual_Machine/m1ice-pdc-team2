package api.entity

import api.request.API_KEY
import api.request.ApiRequester
import org.json.JSONArray
import org.json.JSONObject
import java.util.*

/**
 * The Actor class index all the useful data about a specifics actor.
 */
class Actor(nameParam: String): ApiEntity(), ApiRequester {

    override val id = this.requestSearchByName(nameParam).getInt("id")
    val name: String
    val popularity: Double

    init {
        val personDetails = this.requestPersonDetails()
        this.name = personDetails.getString("name")
        this.popularity = personDetails.getDouble("popularity")
    }

    /**
     * Return all movies where the actor acts in.
     * @return movies : ArrayList of all movies of the actor.
     */
    fun getAllMovies(): ArrayList<Movie> {
        val cast = this.requestPersonMovieCasts()
        val movies = ArrayList<Movie>()

        for (i in 0 until cast.length())
            movies.add(Movie(cast.getJSONObject(i).getInt("id")))

        return movies
    }

    /**
     * Return the ten last movies where the actor acts in.
     * @return movies : ArrayList of the ten latest movies of the actor.
     */
    fun getTenLastMovies(): ArrayList<Movie> {
        val cast = this.requestPersonMovieCasts()

        return this.getCleanedMoviesLIst(cast)
    }

    /**
     * Clean the cast json array send by the API to only take the ten last movies.
     * The API may save some movie even if their not out yet, so we have to clean it up.
     * @param cast : json array of the actor's movie casts
     * @return movies : ArrayList of the ten latest movies of the actor.
     */
    private fun getCleanedMoviesLIst(cast: JSONArray): ArrayList<Movie> {
        var movies: ArrayList<Movie> = ArrayList()

        for (i in 0 until cast.length()) {
            if (cast.getJSONObject(i).getString("character") != null)
                movies.add(Movie(cast.getJSONObject(i), true))
        }
        movies.sortWith(Comparator.comparing(Movie::releaseDate))
        movies.removeIf { movie -> movie.releaseDate.after(Date()) }
        movies = ArrayList(movies.subList(movies.size - 10, movies.size))
        movies.reverse()

        return movies
    }

    /**
     * Request themoviedb API to get the actor's movie casts.
     * @return cast : json array of the actor's movie casts.
     */
    private fun requestPersonMovieCasts(): JSONArray {
        val uri = "https://api.themoviedb.org/3/person/$id/movie_credits?api_key=$API_KEY&language=fr"
        return JSONObject(this.requestApi(uri)).getJSONArray("cast")
    }

    /**
     * Request themoviedb API to search an actor by his name. The first result is
     * always selected.
     * @pram name : the name of the actor we're looking for.
     * @return searchResult : json object of the first search result.
     */
    private fun requestSearchByName(nameParam: String): JSONObject {
        val formattedParam = nameParam.replace(' ', '%')
        val uri = "https://api.themoviedb.org/3/search/person?api_key=$API_KEY&language=fr&query=$formattedParam&page=1&include_adult=false"
        val response = JSONObject(this.requestApi(uri))
        val results = response.getJSONArray("results")

        return results.getJSONObject(0)
    }

    /**
     * Request themoviedb API to get the actor's personal details.
     * @return actorDetails : json object of the actor's details.
     */
    private fun requestPersonDetails(): JSONObject {
        val uri = "https://api.themoviedb.org/3/person/$id?api_key=$API_KEY&language=fr"
        return JSONObject(this.requestApi(uri))
    }
}