package api.entity

/**
 * The super-class of all entity from the API database.
 */
abstract class ApiEntity {

    abstract val id: Int
}