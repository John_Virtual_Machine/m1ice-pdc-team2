package dsl.function;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public abstract class DslFunction {

    public static int DATA_ID = -1;

    /**
     * The id is an unique identifier insert in every data send to
     * the front-end. It's incremented every time it's use.
     * @return DATA_ID.
     */
    public static int getDataId() {
        return ++DATA_ID;
    }

    /**
     * Execute the DSL function to produce data.
     * @return a json object : the data, result of the execution.
     */
    public abstract JSONObject execute();

    /**
     * Format the data sets to be readable by the front end.
     * @param subtitle
     * @param name
     * @param labelParams
     * @param dataParams
     * @return result : the data formatted.
     */
    protected JSONObject getDataFormatted(String subtitle, String name, ArrayList<String> labelParams, ArrayList<Double> dataParams) {
        JSONObject result = new JSONObject();
        JSONObject objectDatasets = new JSONObject();
        JSONObject objectlabels = new JSONObject();
        JSONArray labels = new JSONArray();
        JSONArray datasets = new JSONArray();
        JSONArray data = new JSONArray();

        labelParams.forEach(labelParam -> labels.put(labelParam));
        dataParams.forEach(dataParam -> data.put(dataParam));

        result.put("id", DslFunction.getDataId());
        result.put("subtitle", subtitle);
        result.put("name", name);
        objectlabels.put("labels", labels);
        objectDatasets.put("data",data);
        datasets.put(objectDatasets);
        result.put("datasets", datasets);
        result.put("data", objectlabels);

        return result;
    }

}

