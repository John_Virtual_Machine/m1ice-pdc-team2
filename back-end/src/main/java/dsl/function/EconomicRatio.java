package dsl.function;
import api.entity.Movie;
import org.json.JSONObject;
import java.util.ArrayList;

public class EconomicRatio extends DslFunction {

    ArrayList<Movie> movies;

    public EconomicRatio(ArrayList<Movie> movies) { this.movies = movies; }

    /**
     * Execute the DSL function to produce data.
     * @return a json object : the data, result of the execution.
     */
    @Override
    public JSONObject execute() {

        ArrayList<String> labels = new ArrayList();
        ArrayList<Double> data = new ArrayList();

        this.movies.forEach(movie -> {
            labels.add(movie.getTitle());
            data.add((double) movie.getEconomicRatio());
        });

        return this.getDataFormatted("Economic Ratio", "Economic Ratio", labels, data);
    }
}