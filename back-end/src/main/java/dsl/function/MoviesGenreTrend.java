package dsl.function;

import api.entity.Genre;
import api.entity.Movie;
import api.entity.collection.GenreCollection;
import api.entity.collection.PopularMovieCollection;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MoviesGenreTrend extends DslFunction {

    /**
     * Execute the DSL function to produce data.
     * @return a json object : the data, result of the execution.
     */
    @Override
    public JSONObject execute() {
        JSONArray genres = this.getGenresTrend();
        ArrayList<String> labels = new ArrayList();
        ArrayList<Double> data = new ArrayList();

        for (int i = 0; i < genres.length(); i++) {
            labels.add(genres.getJSONObject(i).getString("name"));
            data.add((double) genres.getJSONObject(i).getFloat("value"));
        }
        return this.getDataFormatted("Movie Genre Trend", "Movie Genre Trend", labels, data);
    }

    /**
     * Get the trend of the most popular movies genres
     * @return a JSONArray with the genre percentage of the most popular movies
     */
    protected JSONArray getGenresTrend() {
        JSONArray genres = new JSONArray();
        // first int: genre ID; second int: occurrences number
        HashMap<Integer, Integer> stats = new HashMap<>();

        int total = getTotalGenresOccurrences(stats);
        getEachGenreTrend(genres, stats, total);

        return genres;
    }

    /**
     * Get the percentage of each genre listed in stats
     * @param genres : JSONArray of the genres of the most popular movies with their ratio
     * @param stats : HashMap of the ratios with key = ID and value = number of occurrences
     * @param total : total number of genres for all movies
     */
    protected void getEachGenreTrend(JSONArray genres, HashMap<Integer, Integer> stats, int total) {
        for(Integer key : stats.keySet()) {
            float value = stats.get(key);
            Genre genre = GenreCollection.find(key);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("name", genre.getName());
            float percent = value/total*100;
            jsonObject.put("value", percent);
            genres.put(jsonObject);
        }
    }

    /**
     * Get the total number of occurrences for each genre in the most popular movies films
     * @param stats : HashMap of the ratios with key = ID and value = number of occurrences
     * @return the total number of genres for all movies in the most popular movies list
     */
    protected int getTotalGenresOccurrences(HashMap<Integer, Integer> stats) {
        int total = 0;
        for(Movie movie : PopularMovieCollection.getAll()) {
            for(Genre genre : movie.getGenres()) {
                int ID = genre.getId();
                if(stats.containsKey(ID))
                    stats.put(ID, stats.get(ID)+1);
                else
                    stats.put(ID, 1);
                total++;
            }
        }
        return total;
    }

}
