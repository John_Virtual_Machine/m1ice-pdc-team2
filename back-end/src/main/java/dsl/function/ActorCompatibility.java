package dsl.function;

import api.entity.Actor;
import api.entity.Movie;
import org.json.JSONObject;
import java.util.ArrayList;

public class ActorCompatibility extends DslFunction {

    Actor actor1;
    Actor actor2;

    public ActorCompatibility(String actorName1, String actorName2) {
        this.actor1 = new Actor(actorName1);
        this.actor2 = new Actor(actorName2);
    }

    /**
     * Execute the DSL function to produce data.
     * @return a json object : the data, result of the execution.
     */
    @Override
    public JSONObject execute() {
        ArrayList<String> labels = new ArrayList();
        ArrayList<Double> data = new ArrayList();

        ArrayList<Movie> actor1Movies = this.actor1.getAllMovies();
        ArrayList<Movie> actor2Movies = this.actor2.getAllMovies();

        actor1Movies.forEach( movie -> {
            if (actor2Movies.contains(movie)) {
                labels.add(movie.getTitle());
                data.add(movie.getPopularity());
            }
        });

        return this.getDataFormatted (
                this.actor1.getName() + ", " + this.actor2.getName(),
                "Actor Compatibility",
                labels,
                data
        );
    }
}