package dsl.function;

import api.entity.Actor;
import org.json.JSONObject;
import java.util.ArrayList;

public class ActorPopularity extends DslFunction {

    Actor actor;

    public ActorPopularity(String actorName) {
        this.actor = new Actor(actorName);
    }

    /**
     * Execute the DSL function to produce data.
     * @return a json object : the data, result of the execution.
     */
    @Override
    public JSONObject execute() {
        ArrayList<String> labels = new ArrayList();
        ArrayList<Double> data = new ArrayList();

        this.actor.getTenLastMovies().forEach(movie -> {
            labels.add(movie.getTitle());
            data.add(movie.getPopularity());
        });

        return this.getDataFormatted(this.actor.getName(), "Actors popularity", labels, data);
    }
}
