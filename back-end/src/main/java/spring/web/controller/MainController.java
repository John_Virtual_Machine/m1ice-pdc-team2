package spring.web.controller;

import dsl.DslEngine;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

@Controller
public class MainController {

	@CrossOrigin(origins = "http://localhost:8080")
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public @ResponseBody String sendData() {

		return DslEngine.getData();
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(value = "/screens", method = RequestMethod.GET)
	public @ResponseBody String sendScreen() {
		try(BufferedReader br = new BufferedReader(new FileReader("STATIC/SCREENS.json"))) {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
				line = br.readLine();
			}
			return sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}
}
