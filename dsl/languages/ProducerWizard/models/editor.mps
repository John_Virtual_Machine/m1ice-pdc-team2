<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:c0445f48-92f0-4103-871e-df152ed0e702(ProducerWizard.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="11" />
    <devkit ref="2677cb18-f558-4e33-bc38-a5139cee06dc(jetbrains.mps.devkit.language-design)" />
  </languages>
  <imports>
    <import index="ldi9" ref="r:502d3867-e3cf-4aeb-921c-09804b9f66ad(ProducerWizard.structure)" />
    <import index="tpen" ref="r:00000000-0000-4000-0000-011c895902c3(jetbrains.mps.baseLanguage.editor)" implicit="true" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi" />
      <concept id="1140524381322" name="jetbrains.mps.lang.editor.structure.CellModel_ListWithRole" flags="ng" index="2czfm3">
        <child id="1140524464360" name="cellLayout" index="2czzBx" />
        <child id="1140524464359" name="emptyCellModel" index="2czzBI" />
      </concept>
      <concept id="1237303669825" name="jetbrains.mps.lang.editor.structure.CellLayout_Indent" flags="nn" index="l2Vlx" />
      <concept id="1237307900041" name="jetbrains.mps.lang.editor.structure.IndentLayoutIndentStyleClassItem" flags="ln" index="lj46D" />
      <concept id="1237308012275" name="jetbrains.mps.lang.editor.structure.IndentLayoutNewLineStyleClassItem" flags="ln" index="ljvvj" />
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="1186403694788" name="jetbrains.mps.lang.editor.structure.ColorStyleClassItem" flags="ln" index="VaVBg">
        <property id="1186403713874" name="color" index="Vb096" />
      </concept>
      <concept id="1186404549998" name="jetbrains.mps.lang.editor.structure.ForegroundColorStyleClassItem" flags="ln" index="VechU" />
      <concept id="1186414536763" name="jetbrains.mps.lang.editor.structure.BooleanStyleSheetItem" flags="ln" index="VOi$J">
        <property id="1186414551515" name="flag" index="VOm3f" />
      </concept>
      <concept id="1233758997495" name="jetbrains.mps.lang.editor.structure.PunctuationLeftStyleClassItem" flags="ln" index="11L4FC" />
      <concept id="1233759184865" name="jetbrains.mps.lang.editor.structure.PunctuationRightStyleClassItem" flags="ln" index="11LMrY" />
      <concept id="1236262245656" name="jetbrains.mps.lang.editor.structure.MatchingLabelStyleClassItem" flags="ln" index="3mYdg7">
        <property id="1238091709220" name="labelName" index="1413C4" />
      </concept>
      <concept id="9122903797312246523" name="jetbrains.mps.lang.editor.structure.StyleReference" flags="ng" index="1wgc9g">
        <reference id="9122903797312247166" name="style" index="1wgcnl" />
      </concept>
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389446423" name="jetbrains.mps.lang.editor.structure.CellModel_Collection" flags="sn" stub="3013115976261988961" index="3EZMnI">
        <child id="1106270802874" name="cellLayout" index="2iSdaV" />
        <child id="1073389446424" name="childCellModel" index="3EZMnx" />
      </concept>
      <concept id="1073389577006" name="jetbrains.mps.lang.editor.structure.CellModel_Constant" flags="sn" stub="3610246225209162225" index="3F0ifn">
        <property id="1073389577007" name="text" index="3F0ifm" />
      </concept>
      <concept id="1073389658414" name="jetbrains.mps.lang.editor.structure.CellModel_Property" flags="sg" stub="730538219796134133" index="3F0A7n" />
      <concept id="1219418625346" name="jetbrains.mps.lang.editor.structure.IStyleContainer" flags="ng" index="3F0Thp">
        <child id="1219418656006" name="styleItem" index="3F10Kt" />
      </concept>
      <concept id="1073389882823" name="jetbrains.mps.lang.editor.structure.CellModel_RefNode" flags="sg" stub="730538219795960754" index="3F1sOY" />
      <concept id="1073390211982" name="jetbrains.mps.lang.editor.structure.CellModel_RefNodeList" flags="sg" stub="2794558372793454595" index="3F2HdR" />
      <concept id="1950447826681509042" name="jetbrains.mps.lang.editor.structure.ApplyStyleClass" flags="lg" index="3Xmtl4">
        <child id="1950447826683828796" name="target" index="3XvnJa" />
      </concept>
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
    </language>
  </registry>
  <node concept="24kQdi" id="1aAGP27rNio">
    <ref role="1XX52x" to="ldi9:5_Hu3f0yfxm" resolve="Analyzer" />
    <node concept="3EZMnI" id="1aAGP27rNip" role="2wV5jI">
      <node concept="l2Vlx" id="1aAGP27rNiq" role="2iSdaV" />
      <node concept="3F0ifn" id="1aAGP27rNir" role="3EZMnx">
        <property role="3F0ifm" value="analyzer" />
        <node concept="3Xmtl4" id="1dwac31boKV" role="3F10Kt">
          <node concept="1wgc9g" id="1dwac31boL3" role="3XvnJa">
            <ref role="1wgcnl" to="tpen:hgVS8CF" resolve="KeyWord" />
          </node>
        </node>
      </node>
      <node concept="3F0A7n" id="1aAGP27rNis" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
        <node concept="ljvvj" id="1aAGP27rNit" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F2HdR" id="5X7X0mzKRJE" role="3EZMnx">
        <ref role="1NtTu8" to="ldi9:1yXE__uVKXM" resolve="commands" />
        <node concept="l2Vlx" id="5X7X0mzKRJH" role="2czzBx" />
        <node concept="ljvvj" id="5X7X0mzL2Pq" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="5X7X0mzL3O0" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="3F0ifn" id="1dwac31boKR" role="2czzBI" />
      </node>
      <node concept="3F0ifn" id="1aAGP27rNiu" role="3EZMnx">
        <property role="3F0ifm" value="end analyzer" />
        <node concept="11L4FC" id="1aAGP27rNiv" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="3mYdg7" id="1aAGP27rNiw" role="3F10Kt">
          <property role="1413C4" value="body-paren" />
        </node>
        <node concept="ljvvj" id="1aAGP27rNix" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="3Xmtl4" id="1dwac31bq3N" role="3F10Kt">
          <node concept="1wgc9g" id="1dwac31bq46" role="3XvnJa">
            <ref role="1wgcnl" to="tpen:hgVS8CF" resolve="KeyWord" />
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="1aAGP27rNmD">
    <property role="3GE5qa" value="command" />
    <ref role="1XX52x" to="ldi9:1aAGP27rN6s" resolve="SerieAudience" />
    <node concept="3EZMnI" id="5yf34gD3iqJ" role="2wV5jI">
      <node concept="l2Vlx" id="5yf34gD3iqK" role="2iSdaV" />
      <node concept="3F0ifn" id="5yf34gD3iqL" role="3EZMnx">
        <property role="3F0ifm" value="series audience" />
        <node concept="3Xmtl4" id="1dwac31bq4b" role="3F10Kt">
          <node concept="1wgc9g" id="1dwac31bq4j" role="3XvnJa">
            <ref role="1wgcnl" to="tpen:hgVS8CF" resolve="KeyWord" />
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="5yf34gD3iqM" role="3EZMnx">
        <property role="3F0ifm" value="(" />
        <node concept="11L4FC" id="5yf34gD3iqN" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="3mYdg7" id="5yf34gD3iqO" role="3F10Kt">
          <property role="1413C4" value="body-paren" />
        </node>
        <node concept="11LMrY" id="5yf34gD3iqP" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F1sOY" id="5yf34gD3iqT" role="3EZMnx">
        <ref role="1NtTu8" to="ldi9:1yXE__uV41b" resolve="stringParam" />
        <node concept="VechU" id="hgVSamN" role="3F10Kt">
          <property role="Vb096" value="DARK_MAGENTA" />
        </node>
      </node>
      <node concept="3F0ifn" id="5yf34gD3iqU" role="3EZMnx">
        <property role="3F0ifm" value=")" />
        <node concept="11L4FC" id="5yf34gD3iqV" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="3mYdg7" id="5yf34gD3iqW" role="3F10Kt">
          <property role="1413C4" value="body-paren" />
        </node>
        <node concept="ljvvj" id="5yf34gD3vkU" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="5X7X0mzL4Np">
    <ref role="1XX52x" to="ldi9:1aAGP27rNjO" resolve="StringParam" />
    <node concept="3F0A7n" id="5X7X0mzL4NT" role="2wV5jI">
      <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
    </node>
  </node>
  <node concept="24kQdi" id="1yXE__uULNc">
    <property role="3GE5qa" value="command" />
    <ref role="1XX52x" to="ldi9:1yXE__uULMg" resolve="ActorPopularity" />
    <node concept="3EZMnI" id="1yXE__uULNd" role="2wV5jI">
      <node concept="l2Vlx" id="1yXE__uULNe" role="2iSdaV" />
      <node concept="3F0ifn" id="1yXE__uULNf" role="3EZMnx">
        <property role="3F0ifm" value="actor popularity" />
        <node concept="3Xmtl4" id="1yXE__uULNg" role="3F10Kt">
          <node concept="1wgc9g" id="1yXE__uULNh" role="3XvnJa">
            <ref role="1wgcnl" to="tpen:hgVS8CF" resolve="KeyWord" />
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="1yXE__uULNi" role="3EZMnx">
        <property role="3F0ifm" value="(" />
        <node concept="11L4FC" id="1yXE__uULNj" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="3mYdg7" id="1yXE__uULNk" role="3F10Kt">
          <property role="1413C4" value="body-paren" />
        </node>
        <node concept="11LMrY" id="1yXE__uULNl" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F1sOY" id="1yXE__uULNm" role="3EZMnx">
        <ref role="1NtTu8" to="ldi9:1yXE__uV417" resolve="stringParam" />
        <node concept="VechU" id="1yXE__uULNn" role="3F10Kt">
          <property role="Vb096" value="DARK_MAGENTA" />
        </node>
      </node>
      <node concept="3F0ifn" id="1yXE__uULNo" role="3EZMnx">
        <property role="3F0ifm" value=")" />
        <node concept="11L4FC" id="1yXE__uULNp" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="3mYdg7" id="1yXE__uULNq" role="3F10Kt">
          <property role="1413C4" value="body-paren" />
        </node>
        <node concept="ljvvj" id="1yXE__uULNr" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="4pglr71k$m$">
    <property role="3GE5qa" value="command" />
    <ref role="1XX52x" to="ldi9:4pglr71k$m5" resolve="EconomicRatio" />
    <node concept="3EZMnI" id="4pglr71k$m_" role="2wV5jI">
      <node concept="l2Vlx" id="4pglr71k$mA" role="2iSdaV" />
      <node concept="3F0ifn" id="4pglr71k$mB" role="3EZMnx">
        <property role="3F0ifm" value="economic ratio" />
        <node concept="3Xmtl4" id="4pglr71k$mC" role="3F10Kt">
          <node concept="1wgc9g" id="4pglr71k$mD" role="3XvnJa">
            <ref role="1wgcnl" to="tpen:hgVS8CF" resolve="KeyWord" />
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="4pglr71k$mE" role="3EZMnx">
        <property role="3F0ifm" value="(" />
        <node concept="11L4FC" id="4pglr71k$mF" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="3mYdg7" id="4pglr71k$mG" role="3F10Kt">
          <property role="1413C4" value="body-paren" />
        </node>
        <node concept="11LMrY" id="4pglr71k$mH" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F1sOY" id="4pglr71k$mI" role="3EZMnx">
        <ref role="1NtTu8" to="ldi9:4pglr71k$mx" resolve="sp1" />
        <node concept="VechU" id="4pglr71k$mJ" role="3F10Kt">
          <property role="Vb096" value="DARK_MAGENTA" />
        </node>
      </node>
      <node concept="3F0ifn" id="4pglr71kUN1" role="3EZMnx">
        <property role="3F0ifm" value="," />
      </node>
      <node concept="3F1sOY" id="4pglr71lex9" role="3EZMnx">
        <ref role="1NtTu8" to="ldi9:4pglr71kUtR" resolve="sp2" />
        <node concept="VechU" id="5RvEIUhBFmr" role="3F10Kt">
          <property role="Vb096" value="DARK_MAGENTA" />
        </node>
      </node>
      <node concept="3F0ifn" id="4pglr71kYhB" role="3EZMnx">
        <property role="3F0ifm" value="," />
      </node>
      <node concept="3F1sOY" id="4pglr71le_o" role="3EZMnx">
        <ref role="1NtTu8" to="ldi9:4pglr71kUuk" resolve="sp3" />
        <node concept="VechU" id="5RvEIUhBFmu" role="3F10Kt">
          <property role="Vb096" value="DARK_MAGENTA" />
        </node>
      </node>
      <node concept="3F0ifn" id="4pglr71kYiO" role="3EZMnx">
        <property role="3F0ifm" value="," />
      </node>
      <node concept="3F1sOY" id="4pglr71leCy" role="3EZMnx">
        <ref role="1NtTu8" to="ldi9:4pglr71kUur" resolve="sp4" />
        <node concept="VechU" id="5RvEIUhBFmx" role="3F10Kt">
          <property role="Vb096" value="DARK_MAGENTA" />
        </node>
      </node>
      <node concept="3F0ifn" id="4pglr71kYo_" role="3EZMnx">
        <property role="3F0ifm" value="," />
      </node>
      <node concept="3F1sOY" id="4pglr71leDR" role="3EZMnx">
        <ref role="1NtTu8" to="ldi9:4pglr71kUu$" resolve="sp5" />
        <node concept="VechU" id="5RvEIUhBFm$" role="3F10Kt">
          <property role="Vb096" value="DARK_MAGENTA" />
        </node>
      </node>
      <node concept="3F0ifn" id="4pglr71k$mK" role="3EZMnx">
        <property role="3F0ifm" value=")" />
        <node concept="11L4FC" id="4pglr71k$mL" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="3mYdg7" id="4pglr71k$mM" role="3F10Kt">
          <property role="1413C4" value="body-paren" />
        </node>
        <node concept="ljvvj" id="4pglr71k$mN" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="6FocJsxuw7a">
    <property role="3GE5qa" value="command" />
    <ref role="1XX52x" to="ldi9:6FocJsxuw5o" resolve="ActorCompatibility" />
    <node concept="3EZMnI" id="6FocJsxuw7B" role="2wV5jI">
      <node concept="l2Vlx" id="6FocJsxuw7C" role="2iSdaV" />
      <node concept="3F0ifn" id="6FocJsxuw7D" role="3EZMnx">
        <property role="3F0ifm" value="actor compatibility" />
        <node concept="3Xmtl4" id="6FocJsxuw7E" role="3F10Kt">
          <node concept="1wgc9g" id="6FocJsxuw7F" role="3XvnJa">
            <ref role="1wgcnl" to="tpen:hgVS8CF" resolve="KeyWord" />
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="6FocJsxuw7G" role="3EZMnx">
        <property role="3F0ifm" value="(" />
        <node concept="11L4FC" id="6FocJsxuw7H" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="3mYdg7" id="6FocJsxuw7I" role="3F10Kt">
          <property role="1413C4" value="body-paren" />
        </node>
        <node concept="11LMrY" id="6FocJsxuw7J" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F1sOY" id="6FocJsxuw7K" role="3EZMnx">
        <ref role="1NtTu8" to="ldi9:6FocJsxuw6f" resolve="sp1" />
        <node concept="VechU" id="6FocJsxuw7L" role="3F10Kt">
          <property role="Vb096" value="DARK_MAGENTA" />
        </node>
      </node>
      <node concept="3F0ifn" id="6FocJsxuw7M" role="3EZMnx">
        <property role="3F0ifm" value="," />
      </node>
      <node concept="3F1sOY" id="6FocJsxuw7N" role="3EZMnx">
        <ref role="1NtTu8" to="ldi9:6FocJsxuw6H" resolve="sp2" />
        <node concept="VechU" id="5RvEIUhBFmo" role="3F10Kt">
          <property role="Vb096" value="DARK_MAGENTA" />
        </node>
      </node>
      <node concept="3F0ifn" id="6FocJsxuw7U" role="3EZMnx">
        <property role="3F0ifm" value=")" />
        <node concept="11L4FC" id="6FocJsxuw7V" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="3mYdg7" id="6FocJsxuw7W" role="3F10Kt">
          <property role="1413C4" value="body-paren" />
        </node>
        <node concept="ljvvj" id="6FocJsxuw7X" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="5RvEIUhBZKZ">
    <property role="3GE5qa" value="command" />
    <ref role="1XX52x" to="ldi9:5RvEIUhBZK8" resolve="MoviesGenreTrend" />
    <node concept="3EZMnI" id="5RvEIUhBZLs" role="2wV5jI">
      <node concept="l2Vlx" id="5RvEIUhBZLt" role="2iSdaV" />
      <node concept="3F0ifn" id="5RvEIUhBZLu" role="3EZMnx">
        <property role="3F0ifm" value="movies genre trend" />
        <node concept="3Xmtl4" id="5RvEIUhBZLv" role="3F10Kt">
          <node concept="1wgc9g" id="5RvEIUhBZLw" role="3XvnJa">
            <ref role="1wgcnl" to="tpen:hgVS8CF" resolve="KeyWord" />
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="5RvEIUhBZLx" role="3EZMnx">
        <property role="3F0ifm" value="(" />
        <node concept="11L4FC" id="5RvEIUhBZLy" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="3mYdg7" id="5RvEIUhBZLz" role="3F10Kt">
          <property role="1413C4" value="body-paren" />
        </node>
        <node concept="11LMrY" id="5RvEIUhBZL$" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="5RvEIUhBZLN" role="3EZMnx">
        <property role="3F0ifm" value=")" />
        <node concept="11L4FC" id="5RvEIUhBZLO" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="3mYdg7" id="5RvEIUhBZLP" role="3F10Kt">
          <property role="1413C4" value="body-paren" />
        </node>
        <node concept="ljvvj" id="5RvEIUhBZLQ" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
    </node>
  </node>
</model>

