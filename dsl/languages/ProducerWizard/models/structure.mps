<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:502d3867-e3cf-4aeb-921c-09804b9f66ad(ProducerWizard.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="6" />
    <devkit ref="78434eb8-b0e5-444b-850d-e7c4ad2da9ab(jetbrains.mps.devkit.aspect.structure)" />
  </languages>
  <imports>
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <property id="4628067390765956807" name="final" index="R5$K2" />
        <property id="4628067390765956802" name="abstract" index="R5$K7" />
        <property id="5092175715804935370" name="conceptAlias" index="34LRSv" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
      </concept>
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <property id="1096454100552" name="rootable" index="19KtqR" />
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="5_Hu3f0yfxm">
    <property role="EcuMT" value="6443938805505914966" />
    <property role="TrG5h" value="Analyzer" />
    <property role="19KtqR" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="2Pif5TcL5t7" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="1TJgyj" id="1yXE__uVKXM" role="1TKVEi">
      <property role="IQ2ns" value="1782768328330121074" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="commands" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="2Pif5TcL5ta" resolve="AbstractCommand" />
    </node>
  </node>
  <node concept="1TIwiD" id="1aAGP27rN6s">
    <property role="EcuMT" value="1343958697267638684" />
    <property role="TrG5h" value="SerieAudience" />
    <property role="34LRSv" value="serie audience" />
    <property role="3GE5qa" value="command" />
    <ref role="1TJDcQ" node="2Pif5TcL5ta" resolve="AbstractCommand" />
    <node concept="1TJgyj" id="1yXE__uV41b" role="1TKVEi">
      <property role="IQ2ns" value="1782768328329936971" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="stringParam" />
      <ref role="20lvS9" node="1aAGP27rNjO" resolve="StringParam" />
    </node>
  </node>
  <node concept="1TIwiD" id="1aAGP27rNjO">
    <property role="EcuMT" value="1343958697267639540" />
    <property role="TrG5h" value="StringParam" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="5X7X0mzL4MX" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="1yXE__uULMg">
    <property role="EcuMT" value="1782768328329862288" />
    <property role="TrG5h" value="ActorPopularity" />
    <property role="3GE5qa" value="command" />
    <property role="34LRSv" value="actor popularity" />
    <ref role="1TJDcQ" node="2Pif5TcL5ta" resolve="AbstractCommand" />
    <node concept="1TJgyj" id="1yXE__uV417" role="1TKVEi">
      <property role="IQ2ns" value="1782768328329936967" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="stringParam" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="1aAGP27rNjO" resolve="StringParam" />
    </node>
  </node>
  <node concept="1TIwiD" id="2Pif5TcL5ta">
    <property role="R5$K7" value="true" />
    <property role="R5$K2" value="false" />
    <property role="TrG5h" value="AbstractCommand" />
    <property role="3GE5qa" value="command" />
    <property role="EcuMT" value="3265739055509559114" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
  </node>
  <node concept="1TIwiD" id="4pglr71k$m5">
    <property role="EcuMT" value="5066643802732774789" />
    <property role="3GE5qa" value="command" />
    <property role="TrG5h" value="EconomicRatio" />
    <property role="34LRSv" value="economic ratio" />
    <ref role="1TJDcQ" node="2Pif5TcL5ta" resolve="AbstractCommand" />
    <node concept="1TJgyj" id="4pglr71k$mx" role="1TKVEi">
      <property role="IQ2ns" value="5066643802732774817" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="sp1" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="1aAGP27rNjO" resolve="StringParam" />
    </node>
    <node concept="1TJgyj" id="4pglr71kUtR" role="1TKVEi">
      <property role="IQ2ns" value="5066643802732865399" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="sp2" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="1aAGP27rNjO" resolve="StringParam" />
    </node>
    <node concept="1TJgyj" id="4pglr71kUuk" role="1TKVEi">
      <property role="IQ2ns" value="5066643802732865428" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="sp3" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="1aAGP27rNjO" resolve="StringParam" />
    </node>
    <node concept="1TJgyj" id="4pglr71kUur" role="1TKVEi">
      <property role="IQ2ns" value="5066643802732865435" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="sp4" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="1aAGP27rNjO" resolve="StringParam" />
    </node>
    <node concept="1TJgyj" id="4pglr71kUu$" role="1TKVEi">
      <property role="IQ2ns" value="5066643802732865444" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="sp5" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="1aAGP27rNjO" resolve="StringParam" />
    </node>
  </node>
  <node concept="1TIwiD" id="6FocJsxuw5o">
    <property role="EcuMT" value="7698959599989817688" />
    <property role="3GE5qa" value="command" />
    <property role="TrG5h" value="ActorCompatibility" />
    <property role="34LRSv" value="actor compatibility" />
    <ref role="1TJDcQ" node="2Pif5TcL5ta" resolve="AbstractCommand" />
    <node concept="1TJgyj" id="6FocJsxuw6f" role="1TKVEi">
      <property role="IQ2ns" value="7698959599989817743" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="sp1" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="1aAGP27rNjO" resolve="StringParam" />
    </node>
    <node concept="1TJgyj" id="6FocJsxuw6H" role="1TKVEi">
      <property role="IQ2ns" value="7698959599989817773" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="sp2" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="1aAGP27rNjO" resolve="StringParam" />
    </node>
  </node>
  <node concept="1TIwiD" id="5RvEIUhBZK8">
    <property role="EcuMT" value="6764313106955893768" />
    <property role="3GE5qa" value="command" />
    <property role="TrG5h" value="MoviesGenreTrend" />
    <property role="34LRSv" value="movies genre trend" />
    <ref role="1TJDcQ" node="2Pif5TcL5ta" resolve="AbstractCommand" />
  </node>
</model>

