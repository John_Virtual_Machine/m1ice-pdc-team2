<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:dec5abda-883b-4963-b741-cd78394f6e06(main@generator)">
  <persistence version="9" />
  <languages>
    <use id="f2801650-65d5-424e-bb1b-463a8781b786" name="jetbrains.mps.baseLanguage.javadoc" version="2" />
    <use id="b401a680-8325-4110-8fd3-84331ff25bef" name="jetbrains.mps.lang.generator" version="0" />
    <use id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage" version="6" />
    <devkit ref="a2eb3a43-fcc2-4200-80dc-c60110c4862d(jetbrains.mps.devkit.templates)" />
  </languages>
  <imports>
    <import index="ldi9" ref="r:502d3867-e3cf-4aeb-921c-09804b9f66ad(ProducerWizard.structure)" />
    <import index="guwi" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.io(JDK/)" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" />
    <import index="tpee" ref="r:00000000-0000-4000-0000-011c895902ca(jetbrains.mps.baseLanguage.structure)" />
    <import index="33ny" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.util(JDK/)" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1082485599095" name="jetbrains.mps.baseLanguage.structure.BlockStatement" flags="nn" index="9aQIb">
        <child id="1082485599096" name="statements" index="9aQI4" />
      </concept>
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="4836112446988635817" name="jetbrains.mps.baseLanguage.structure.UndefinedType" flags="in" index="2jxLKc" />
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1465982738277781862" name="jetbrains.mps.baseLanguage.structure.PlaceholderMember" flags="ng" index="2tJIrI" />
      <concept id="1188220165133" name="jetbrains.mps.baseLanguage.structure.ArrayLiteral" flags="nn" index="2BsdOp">
        <child id="1188220173759" name="item" index="2BsfMF" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1070534644030" name="jetbrains.mps.baseLanguage.structure.BooleanType" flags="in" index="10P_77" />
      <concept id="1070534760951" name="jetbrains.mps.baseLanguage.structure.ArrayType" flags="in" index="10Q1$e">
        <child id="1070534760952" name="componentType" index="10Q1$1" />
      </concept>
      <concept id="1068390468198" name="jetbrains.mps.baseLanguage.structure.ClassConcept" flags="ig" index="312cEu" />
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <property id="4276006055363816570" name="isSynchronized" index="od$2w" />
        <property id="1181808852946" name="isFinal" index="DiZV1" />
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123165" name="jetbrains.mps.baseLanguage.structure.InstanceMethodDeclaration" flags="ig" index="3clFb_">
        <property id="1178608670077" name="isAbstract" index="1EzhhJ" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123137" name="jetbrains.mps.baseLanguage.structure.BooleanConstant" flags="nn" index="3clFbT">
        <property id="1068580123138" name="value" index="3clFbU" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1212685548494" name="jetbrains.mps.baseLanguage.structure.ClassCreator" flags="nn" index="1pGfFk">
        <child id="1212687122400" name="typeParameter" index="1pMfVU" />
      </concept>
      <concept id="1107461130800" name="jetbrains.mps.baseLanguage.structure.Classifier" flags="ng" index="3pOWGL">
        <child id="5375687026011219971" name="member" index="jymVt" unordered="true" />
      </concept>
      <concept id="7812454656619025416" name="jetbrains.mps.baseLanguage.structure.MethodDeclaration" flags="ng" index="1rXfSm">
        <property id="8355037393041754995" name="isNative" index="2aFKle" />
      </concept>
      <concept id="7812454656619025412" name="jetbrains.mps.baseLanguage.structure.LocalMethodCall" flags="nn" index="1rXfSq" />
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
        <child id="1109201940907" name="parameter" index="11_B2D" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
      <concept id="1146644623116" name="jetbrains.mps.baseLanguage.structure.PrivateVisibility" flags="nn" index="3Tm6S6" />
    </language>
    <language id="b401a680-8325-4110-8fd3-84331ff25bef" name="jetbrains.mps.lang.generator">
      <concept id="1095416546421" name="jetbrains.mps.lang.generator.structure.MappingConfiguration" flags="ig" index="bUwia">
        <child id="1200911492601" name="mappingLabel" index="2rTMjI" />
        <child id="1167514678247" name="rootMappingRule" index="3lj3bC" />
      </concept>
      <concept id="1168619357332" name="jetbrains.mps.lang.generator.structure.RootTemplateAnnotation" flags="lg" index="n94m4">
        <reference id="1168619429071" name="applicableConcept" index="n9lRv" />
      </concept>
      <concept id="1200911316486" name="jetbrains.mps.lang.generator.structure.MappingLabelDeclaration" flags="lg" index="2rT7sh">
        <reference id="1200911342686" name="sourceConcept" index="2rTdP9" />
        <reference id="1200913004646" name="targetConcept" index="2rZz_L" />
      </concept>
      <concept id="1167169188348" name="jetbrains.mps.lang.generator.structure.TemplateFunctionParameter_sourceNode" flags="nn" index="30H73N" />
      <concept id="1167169308231" name="jetbrains.mps.lang.generator.structure.BaseMappingRule" flags="ng" index="30H$t8">
        <reference id="1167169349424" name="applicableConcept" index="30HIoZ" />
      </concept>
      <concept id="1087833241328" name="jetbrains.mps.lang.generator.structure.PropertyMacro" flags="ln" index="17Uvod">
        <child id="1167756362303" name="propertyValueFunction" index="3zH0cK" />
      </concept>
      <concept id="1087833466690" name="jetbrains.mps.lang.generator.structure.NodeMacro" flags="lg" index="17VmuZ">
        <reference id="1200912223215" name="mappingLabel" index="2rW$FS" />
      </concept>
      <concept id="1167514355419" name="jetbrains.mps.lang.generator.structure.Root_MappingRule" flags="lg" index="3lhOvk">
        <reference id="1167514355421" name="template" index="3lhOvi" />
      </concept>
      <concept id="1167756080639" name="jetbrains.mps.lang.generator.structure.PropertyMacro_GetPropertyValue" flags="in" index="3zFVjK" />
      <concept id="1167951910403" name="jetbrains.mps.lang.generator.structure.SourceSubstituteMacro_SourceNodesQuery" flags="in" index="3JmXsc" />
      <concept id="1118786554307" name="jetbrains.mps.lang.generator.structure.LoopMacro" flags="ln" index="1WS0z7">
        <child id="1167952069335" name="sourceNodesQuery" index="3Jn$fo" />
      </concept>
    </language>
    <language id="fd392034-7849-419d-9071-12563d152375" name="jetbrains.mps.baseLanguage.closures">
      <concept id="1199569711397" name="jetbrains.mps.baseLanguage.closures.structure.ClosureLiteral" flags="nn" index="1bVj0M">
        <child id="1199569906740" name="parameter" index="1bW2Oz" />
        <child id="1199569916463" name="body" index="1bW5cS" />
      </concept>
    </language>
    <language id="f2801650-65d5-424e-bb1b-463a8781b786" name="jetbrains.mps.baseLanguage.javadoc">
      <concept id="5858074156537516430" name="jetbrains.mps.baseLanguage.javadoc.structure.ReturnBlockDocTag" flags="ng" index="x79VA">
        <property id="5858074156537516431" name="text" index="x79VB" />
      </concept>
      <concept id="5349172909345501395" name="jetbrains.mps.baseLanguage.javadoc.structure.BaseDocComment" flags="ng" index="P$AiS">
        <child id="8465538089690331502" name="body" index="TZ5H$" />
        <child id="5383422241790532083" name="tags" index="3nqlJM" />
      </concept>
      <concept id="5349172909345532724" name="jetbrains.mps.baseLanguage.javadoc.structure.MethodDocComment" flags="ng" index="P$JXv" />
      <concept id="8465538089690331500" name="jetbrains.mps.baseLanguage.javadoc.structure.CommentLine" flags="ng" index="TZ5HA">
        <child id="8970989240999019149" name="part" index="1dT_Ay" />
      </concept>
      <concept id="8970989240999019143" name="jetbrains.mps.baseLanguage.javadoc.structure.TextCommentLinePart" flags="ng" index="1dT_AC">
        <property id="8970989240999019144" name="text" index="1dT_AB" />
      </concept>
    </language>
    <language id="d7706f63-9be2-479c-a3da-ae92af1e64d5" name="jetbrains.mps.lang.generator.generationContext">
      <concept id="1218047638031" name="jetbrains.mps.lang.generator.generationContext.structure.GenerationContextOp_CreateUniqueName" flags="nn" index="2piZGk">
        <child id="1218047638032" name="baseName" index="2piZGb" />
      </concept>
      <concept id="1216860049635" name="jetbrains.mps.lang.generator.generationContext.structure.TemplateFunctionParameter_generationContext" flags="nn" index="1iwH7S" />
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="2396822768958367367" name="jetbrains.mps.lang.smodel.structure.AbstractTypeCastExpression" flags="nn" index="$5XWr">
        <child id="6733348108486823193" name="leftExpression" index="1m5AlR" />
        <child id="3906496115198199033" name="conceptArgument" index="3oSUPX" />
      </concept>
      <concept id="1139621453865" name="jetbrains.mps.lang.smodel.structure.Node_IsInstanceOfOperation" flags="nn" index="1mIQ4w">
        <child id="1177027386292" name="conceptArgument" index="cj9EA" />
      </concept>
      <concept id="1140137987495" name="jetbrains.mps.lang.smodel.structure.SNodeTypeCastExpression" flags="nn" index="1PxgMI">
        <property id="1238684351431" name="asCast" index="1BlNFB" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="3364660638048049750" name="jetbrains.mps.lang.core.structure.PropertyAttribute" flags="ng" index="A9Btg">
        <property id="1757699476691236117" name="propertyName" index="2qtEX9" />
        <property id="1341860900487648621" name="propertyId" index="P4ACc" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1204796164442" name="jetbrains.mps.baseLanguage.collections.structure.InternalSequenceOperation" flags="nn" index="23sCx2">
        <child id="1204796294226" name="closure" index="23t8la" />
      </concept>
      <concept id="1203518072036" name="jetbrains.mps.baseLanguage.collections.structure.SmartClosureParameterDeclaration" flags="ig" index="Rh6nW" />
      <concept id="1202120902084" name="jetbrains.mps.baseLanguage.collections.structure.WhereOperation" flags="nn" index="3zZkjj" />
    </language>
  </registry>
  <node concept="bUwia" id="5_Hu3f0yf7c">
    <property role="TrG5h" value="main" />
    <node concept="2rT7sh" id="i474uTf" role="2rTMjI">
      <property role="TrG5h" value="ActorPopularityMethod" />
      <ref role="2rZz_L" to="tpee:fzclF8t" resolve="InstanceMethodDeclaration" />
      <ref role="2rTdP9" to="ldi9:1yXE__uULMg" resolve="ActorPopularity" />
    </node>
    <node concept="2rT7sh" id="diSKvcrG4R" role="2rTMjI">
      <property role="TrG5h" value="EconomicRatioMethod" />
      <ref role="2rTdP9" to="ldi9:4pglr71k$m5" resolve="EconomicRatio" />
      <ref role="2rZz_L" to="tpee:fzclF8t" resolve="InstanceMethodDeclaration" />
    </node>
    <node concept="2rT7sh" id="6FocJsxuxe4" role="2rTMjI">
      <property role="TrG5h" value="ActorCompatibilityMethod" />
      <ref role="2rZz_L" to="tpee:fzclF8t" resolve="InstanceMethodDeclaration" />
      <ref role="2rTdP9" to="ldi9:6FocJsxuw5o" resolve="ActorCompatibility" />
    </node>
    <node concept="3lhOvk" id="5_Hu3f0yxSM" role="3lj3bC">
      <ref role="30HIoZ" to="ldi9:5_Hu3f0yfxm" resolve="Analyzer" />
      <ref role="3lhOvi" node="5_Hu3f0yqgH" resolve="DslTranslator" />
    </node>
  </node>
  <node concept="312cEu" id="5_Hu3f0yqgH">
    <property role="TrG5h" value="DslTranslator" />
    <node concept="2tJIrI" id="7nT1Vzx176x" role="jymVt" />
    <node concept="3clFb_" id="79RuHsZ$eyE" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="getDesiredActorPopularity" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="79RuHsZ$eyH" role="3clF47">
        <node concept="3cpWs8" id="1dwac31aLiz" role="3cqZAp">
          <node concept="3cpWsn" id="1dwac31aLiD" role="3cpWs9">
            <property role="TrG5h" value="actorPopularities" />
            <node concept="3uibUv" id="1dwac31aLiF" role="1tU5fm">
              <ref role="3uigEE" to="33ny:~ArrayList" resolve="ArrayList" />
              <node concept="17QB3L" id="1dwac31aLsr" role="11_B2D" />
            </node>
            <node concept="2ShNRf" id="1dwac31aLPj" role="33vP2m">
              <node concept="1pGfFk" id="1dwac31aN3d" role="2ShVmc">
                <ref role="37wK5l" to="33ny:~ArrayList.&lt;init&gt;()" resolve="ArrayList" />
                <node concept="17QB3L" id="1dwac31aNbO" role="1pMfVU" />
              </node>
            </node>
          </node>
        </node>
        <node concept="9aQIb" id="i474U7B" role="3cqZAp">
          <node concept="3clFbS" id="i474U7C" role="9aQI4">
            <node concept="3clFbF" id="79RuHsZ$yxQ" role="3cqZAp">
              <node concept="2OqwBi" id="1dwac31aOnB" role="3clFbG">
                <node concept="37vLTw" id="1dwac31aNpG" role="2Oq$k0">
                  <ref role="3cqZAo" node="1dwac31aLiD" resolve="actorPopularities" />
                </node>
                <node concept="liA8E" id="1dwac31aP$c" role="2OqNvi">
                  <ref role="37wK5l" to="33ny:~ArrayList.add(java.lang.Object):boolean" resolve="add" />
                  <node concept="1rXfSq" id="1dwac31aPLY" role="37wK5m">
                    <ref role="37wK5l" node="2RDssu5VrPZ" resolve="actorPopularity" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="1WS0z7" id="i474U8p" role="lGtFl">
            <node concept="3JmXsc" id="i474U8q" role="3Jn$fo">
              <node concept="3clFbS" id="i474U8r" role="2VODD2">
                <node concept="3clFbF" id="i474U8s" role="3cqZAp">
                  <node concept="2OqwBi" id="3GDlSEIPmUV" role="3clFbG">
                    <node concept="2OqwBi" id="i474U8t" role="2Oq$k0">
                      <node concept="30H73N" id="i474U8u" role="2Oq$k0" />
                      <node concept="3Tsc0h" id="3GDlSEIOXnf" role="2OqNvi">
                        <ref role="3TtcxE" to="ldi9:1yXE__uVKXM" resolve="commands" />
                      </node>
                    </node>
                    <node concept="3zZkjj" id="3GDlSEIPoiH" role="2OqNvi">
                      <node concept="1bVj0M" id="3GDlSEIPoiJ" role="23t8la">
                        <node concept="3clFbS" id="3GDlSEIPoiK" role="1bW5cS">
                          <node concept="3clFbF" id="3GDlSEIPo_B" role="3cqZAp">
                            <node concept="2OqwBi" id="3GDlSEIPoP3" role="3clFbG">
                              <node concept="37vLTw" id="3GDlSEIPo_A" role="2Oq$k0">
                                <ref role="3cqZAo" node="3GDlSEIPoiL" resolve="it" />
                              </node>
                              <node concept="1mIQ4w" id="3GDlSEIPp60" role="2OqNvi">
                                <node concept="chp4Y" id="3GDlSEIPpiF" role="cj9EA">
                                  <ref role="cht4Q" to="ldi9:1yXE__uULMg" resolve="ActorPopularity" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="Rh6nW" id="3GDlSEIPoiL" role="1bW2Oz">
                          <property role="TrG5h" value="it" />
                          <node concept="2jxLKc" id="3GDlSEIPoiM" role="1tU5fm" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="1dwac31aQiQ" role="3cqZAp">
          <node concept="37vLTw" id="1dwac31aQtH" role="3cqZAk">
            <ref role="3cqZAo" node="1dwac31aLiD" resolve="actorPopularities" />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="79RuHsZ$e2Y" role="1B3o_S" />
      <node concept="3uibUv" id="1dwac31aKJg" role="3clF45">
        <ref role="3uigEE" to="33ny:~ArrayList" resolve="ArrayList" />
        <node concept="17QB3L" id="1dwac31aL7p" role="11_B2D" />
      </node>
      <node concept="P$JXv" id="7nT1Vzx18md" role="lGtFl">
        <node concept="TZ5HA" id="7nT1Vzx18me" role="TZ5H$">
          <node concept="1dT_AC" id="7nT1Vzx18mf" role="1dT_Ay">
            <property role="1dT_AB" value="Get all actor names for which the user would know the popularity." />
          </node>
        </node>
        <node concept="TZ5HA" id="7nT1Vzx19Y8" role="TZ5H$">
          <node concept="1dT_AC" id="7nT1Vzx19Y9" role="1dT_Ay">
            <property role="1dT_AB" value="" />
          </node>
        </node>
        <node concept="x79VA" id="7nT1Vzx1acY" role="3nqlJM">
          <property role="x79VB" value=" actorPopularities : A list of actor names." />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="4pglr71m9JM" role="jymVt" />
    <node concept="2tJIrI" id="7nT1Vzx1avo" role="jymVt" />
    <node concept="3clFb_" id="4pglr71mbsC" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="getDesiredEconomicRatio" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="4pglr71mbsD" role="3clF47">
        <node concept="3cpWs8" id="4pglr71mbsE" role="3cqZAp">
          <node concept="3cpWsn" id="4pglr71mbsF" role="3cpWs9">
            <property role="TrG5h" value="economicRatios" />
            <node concept="3uibUv" id="4pglr71mbsG" role="1tU5fm">
              <ref role="3uigEE" to="33ny:~ArrayList" resolve="ArrayList" />
              <node concept="10Q1$e" id="4pglr71msj$" role="11_B2D">
                <node concept="17QB3L" id="4pglr71mbsH" role="10Q1$1" />
              </node>
            </node>
            <node concept="2ShNRf" id="4pglr71mbsI" role="33vP2m">
              <node concept="1pGfFk" id="4pglr71mbsJ" role="2ShVmc">
                <ref role="37wK5l" to="33ny:~ArrayList.&lt;init&gt;()" resolve="ArrayList" />
                <node concept="10Q1$e" id="4pglr71mts0" role="1pMfVU">
                  <node concept="17QB3L" id="4pglr71mbsK" role="10Q1$1" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="9aQIb" id="4pglr71mbsL" role="3cqZAp">
          <node concept="3clFbS" id="4pglr71mbsM" role="9aQI4">
            <node concept="3clFbF" id="4pglr71mbsN" role="3cqZAp">
              <node concept="2OqwBi" id="4pglr71mbsO" role="3clFbG">
                <node concept="37vLTw" id="4pglr71mbsP" role="2Oq$k0">
                  <ref role="3cqZAo" node="4pglr71mbsF" resolve="economicRatios" />
                </node>
                <node concept="liA8E" id="4pglr71mbsQ" role="2OqNvi">
                  <ref role="37wK5l" to="33ny:~ArrayList.add(java.lang.Object):boolean" resolve="add" />
                  <node concept="1rXfSq" id="4pglr71mbsR" role="37wK5m">
                    <ref role="37wK5l" node="4pglr71lurg" resolve="economicRatio" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="1WS0z7" id="4pglr71mbsS" role="lGtFl">
            <node concept="3JmXsc" id="4pglr71mbsT" role="3Jn$fo">
              <node concept="3clFbS" id="4pglr71mbsU" role="2VODD2">
                <node concept="3clFbF" id="4pglr71mbsV" role="3cqZAp">
                  <node concept="2OqwBi" id="4pglr71mbsW" role="3clFbG">
                    <node concept="2OqwBi" id="4pglr71mbsX" role="2Oq$k0">
                      <node concept="30H73N" id="4pglr71mbsY" role="2Oq$k0" />
                      <node concept="3Tsc0h" id="4pglr71mbsZ" role="2OqNvi">
                        <ref role="3TtcxE" to="ldi9:1yXE__uVKXM" resolve="commands" />
                      </node>
                    </node>
                    <node concept="3zZkjj" id="4pglr71mbt0" role="2OqNvi">
                      <node concept="1bVj0M" id="4pglr71mbt1" role="23t8la">
                        <node concept="3clFbS" id="4pglr71mbt2" role="1bW5cS">
                          <node concept="3clFbF" id="4pglr71mbt3" role="3cqZAp">
                            <node concept="2OqwBi" id="4pglr71mbt4" role="3clFbG">
                              <node concept="37vLTw" id="4pglr71mbt5" role="2Oq$k0">
                                <ref role="3cqZAo" node="4pglr71mbt8" resolve="it" />
                              </node>
                              <node concept="1mIQ4w" id="4pglr71mbt6" role="2OqNvi">
                                <node concept="chp4Y" id="4pglr71mvKI" role="cj9EA">
                                  <ref role="cht4Q" to="ldi9:4pglr71k$m5" resolve="EconomicRatio" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="Rh6nW" id="4pglr71mbt8" role="1bW2Oz">
                          <property role="TrG5h" value="it" />
                          <node concept="2jxLKc" id="4pglr71mbt9" role="1tU5fm" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="4pglr71mbta" role="3cqZAp">
          <node concept="37vLTw" id="4pglr71mbtb" role="3cqZAk">
            <ref role="3cqZAo" node="4pglr71mbsF" resolve="economicRatios" />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="4pglr71mbtc" role="1B3o_S" />
      <node concept="3uibUv" id="4pglr71mbtd" role="3clF45">
        <ref role="3uigEE" to="33ny:~ArrayList" resolve="ArrayList" />
        <node concept="10Q1$e" id="4pglr71mcCL" role="11_B2D">
          <node concept="17QB3L" id="4pglr71mbte" role="10Q1$1" />
        </node>
      </node>
      <node concept="P$JXv" id="7nT1Vzx1bLm" role="lGtFl">
        <node concept="TZ5HA" id="7nT1Vzx1bLn" role="TZ5H$">
          <node concept="1dT_AC" id="7nT1Vzx1bLo" role="1dT_Ay">
            <property role="1dT_AB" value="Get all movie titles set for which the user would know the economic " />
          </node>
        </node>
        <node concept="TZ5HA" id="7nT1Vzx1dj1" role="TZ5H$">
          <node concept="1dT_AC" id="7nT1Vzx1dj2" role="1dT_Ay">
            <property role="1dT_AB" value="ratio." />
          </node>
        </node>
        <node concept="TZ5HA" id="7nT1Vzx1dy1" role="TZ5H$">
          <node concept="1dT_AC" id="7nT1Vzx1dy2" role="1dT_Ay">
            <property role="1dT_AB" value="" />
          </node>
        </node>
        <node concept="x79VA" id="7nT1Vzx1dL3" role="3nqlJM">
          <property role="x79VB" value="economicRatios : A list of movie title array." />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="6FocJsxvEKT" role="jymVt" />
    <node concept="2tJIrI" id="7nT1Vzx1e3v" role="jymVt" />
    <node concept="3clFb_" id="6FocJsxvIul" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="getDesiredActorCompatibility" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="6FocJsxvIuo" role="3clF47">
        <node concept="3cpWs8" id="6FocJsxvJGA" role="3cqZAp">
          <node concept="3cpWsn" id="6FocJsxvJGB" role="3cpWs9">
            <property role="TrG5h" value="actorCompatibilities" />
            <node concept="3uibUv" id="6FocJsxvJG$" role="1tU5fm">
              <ref role="3uigEE" to="33ny:~ArrayList" resolve="ArrayList" />
              <node concept="10Q1$e" id="6FocJsxvJNd" role="11_B2D">
                <node concept="17QB3L" id="6FocJsxvJMy" role="10Q1$1" />
              </node>
            </node>
            <node concept="2ShNRf" id="6FocJsxvJVA" role="33vP2m">
              <node concept="1pGfFk" id="6FocJsxvLlr" role="2ShVmc">
                <ref role="37wK5l" to="33ny:~ArrayList.&lt;init&gt;()" resolve="ArrayList" />
                <node concept="10Q1$e" id="6FocJsxvLKF" role="1pMfVU">
                  <node concept="17QB3L" id="6FocJsxvLzS" role="10Q1$1" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="9aQIb" id="6FocJsxvOYm" role="3cqZAp">
          <node concept="3clFbS" id="6FocJsxvOYn" role="9aQI4">
            <node concept="3clFbF" id="6FocJsxvOYo" role="3cqZAp">
              <node concept="2OqwBi" id="6FocJsxvOYp" role="3clFbG">
                <node concept="liA8E" id="6FocJsxvOYr" role="2OqNvi">
                  <ref role="37wK5l" to="33ny:~ArrayList.add(java.lang.Object):boolean" resolve="add" />
                  <node concept="1rXfSq" id="6FocJsxvOYs" role="37wK5m">
                    <ref role="37wK5l" node="6FocJsxuLan" resolve="actorCompatibility" />
                  </node>
                </node>
                <node concept="37vLTw" id="6FocJsxvSDr" role="2Oq$k0">
                  <ref role="3cqZAo" node="6FocJsxvJGB" resolve="actorCompatibilities" />
                </node>
              </node>
            </node>
          </node>
          <node concept="1WS0z7" id="6FocJsxvOYt" role="lGtFl">
            <node concept="3JmXsc" id="6FocJsxvOYu" role="3Jn$fo">
              <node concept="3clFbS" id="6FocJsxvOYv" role="2VODD2">
                <node concept="3clFbF" id="6FocJsxvOYw" role="3cqZAp">
                  <node concept="2OqwBi" id="6FocJsxvOYx" role="3clFbG">
                    <node concept="2OqwBi" id="6FocJsxvOYy" role="2Oq$k0">
                      <node concept="30H73N" id="6FocJsxvOYz" role="2Oq$k0" />
                      <node concept="3Tsc0h" id="6FocJsxvOY$" role="2OqNvi">
                        <ref role="3TtcxE" to="ldi9:1yXE__uVKXM" resolve="commands" />
                      </node>
                    </node>
                    <node concept="3zZkjj" id="6FocJsxvOY_" role="2OqNvi">
                      <node concept="1bVj0M" id="6FocJsxvOYA" role="23t8la">
                        <node concept="3clFbS" id="6FocJsxvOYB" role="1bW5cS">
                          <node concept="3clFbF" id="6FocJsxvOYC" role="3cqZAp">
                            <node concept="2OqwBi" id="6FocJsxvOYD" role="3clFbG">
                              <node concept="37vLTw" id="6FocJsxvOYE" role="2Oq$k0">
                                <ref role="3cqZAo" node="6FocJsxvOYH" resolve="it" />
                              </node>
                              <node concept="1mIQ4w" id="6FocJsxvOYF" role="2OqNvi">
                                <node concept="chp4Y" id="6FocJsxvS5P" role="cj9EA">
                                  <ref role="cht4Q" to="ldi9:6FocJsxuw5o" resolve="ActorCompatibility" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="Rh6nW" id="6FocJsxvOYH" role="1bW2Oz">
                          <property role="TrG5h" value="it" />
                          <node concept="2jxLKc" id="6FocJsxvOYI" role="1tU5fm" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="6FocJsxvNXt" role="3cqZAp" />
        <node concept="3cpWs6" id="6FocJsxvLWf" role="3cqZAp">
          <node concept="37vLTw" id="6FocJsxvMWZ" role="3cqZAk">
            <ref role="3cqZAo" node="6FocJsxvJGB" resolve="actorCompatibilities" />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="6FocJsxvHaE" role="1B3o_S" />
      <node concept="3uibUv" id="6FocJsxvIjd" role="3clF45">
        <ref role="3uigEE" to="33ny:~ArrayList" resolve="ArrayList" />
        <node concept="10Q1$e" id="6FocJsxvIps" role="11_B2D">
          <node concept="17QB3L" id="6FocJsxvIp0" role="10Q1$1" />
        </node>
      </node>
      <node concept="P$JXv" id="7nT1Vzx1flo" role="lGtFl">
        <node concept="TZ5HA" id="7nT1Vzx1flp" role="TZ5H$">
          <node concept="1dT_AC" id="7nT1Vzx1flq" role="1dT_Ay">
            <property role="1dT_AB" value="Get all actor set for which the user would know the compatibility between" />
          </node>
        </node>
        <node concept="TZ5HA" id="7nT1Vzx1gvV" role="TZ5H$">
          <node concept="1dT_AC" id="7nT1Vzx1gvW" role="1dT_Ay">
            <property role="1dT_AB" value="then." />
          </node>
        </node>
        <node concept="TZ5HA" id="7nT1Vzx1gIV" role="TZ5H$">
          <node concept="1dT_AC" id="7nT1Vzx1gIW" role="1dT_Ay">
            <property role="1dT_AB" value="" />
          </node>
        </node>
        <node concept="x79VA" id="7nT1Vzx1gXX" role="3nqlJM">
          <property role="x79VB" value="actorCompatibilities : A list of actor name array." />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="5RvEIUhC0bP" role="jymVt" />
    <node concept="2tJIrI" id="7nT1Vzx1hgL" role="jymVt" />
    <node concept="3clFb_" id="5RvEIUhC2AL" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="isMoviesGenreTrendDesired" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="5RvEIUhC2AO" role="3clF47">
        <node concept="3cpWs8" id="5RvEIUhC3LY" role="3cqZAp">
          <node concept="3cpWsn" id="5RvEIUhC3M1" role="3cpWs9">
            <property role="TrG5h" value="desired" />
            <node concept="10P_77" id="5RvEIUhC3LX" role="1tU5fm" />
            <node concept="3clFbT" id="5RvEIUhC3Nx" role="33vP2m">
              <property role="3clFbU" value="false" />
            </node>
          </node>
        </node>
        <node concept="9aQIb" id="5RvEIUhCbeH" role="3cqZAp">
          <node concept="3clFbS" id="5RvEIUhCbeI" role="9aQI4">
            <node concept="3clFbF" id="5RvEIUhCbeJ" role="3cqZAp">
              <node concept="37vLTI" id="5RvEIUhCdGx" role="3clFbG">
                <node concept="3clFbT" id="5RvEIUhCdH7" role="37vLTx">
                  <property role="3clFbU" value="true" />
                </node>
                <node concept="37vLTw" id="5RvEIUhCd6d" role="37vLTJ">
                  <ref role="3cqZAo" node="5RvEIUhC3M1" resolve="desired" />
                </node>
              </node>
            </node>
          </node>
          <node concept="1WS0z7" id="5RvEIUhCbeO" role="lGtFl">
            <node concept="3JmXsc" id="5RvEIUhCbeP" role="3Jn$fo">
              <node concept="3clFbS" id="5RvEIUhCbeQ" role="2VODD2">
                <node concept="3clFbF" id="5RvEIUhCbeR" role="3cqZAp">
                  <node concept="2OqwBi" id="5RvEIUhCbeS" role="3clFbG">
                    <node concept="2OqwBi" id="5RvEIUhCbeT" role="2Oq$k0">
                      <node concept="30H73N" id="5RvEIUhCbeU" role="2Oq$k0" />
                      <node concept="3Tsc0h" id="5RvEIUhCbeV" role="2OqNvi">
                        <ref role="3TtcxE" to="ldi9:1yXE__uVKXM" resolve="commands" />
                      </node>
                    </node>
                    <node concept="3zZkjj" id="5RvEIUhCbeW" role="2OqNvi">
                      <node concept="1bVj0M" id="5RvEIUhCbeX" role="23t8la">
                        <node concept="3clFbS" id="5RvEIUhCbeY" role="1bW5cS">
                          <node concept="3clFbF" id="5RvEIUhCbeZ" role="3cqZAp">
                            <node concept="2OqwBi" id="5RvEIUhCbf0" role="3clFbG">
                              <node concept="37vLTw" id="5RvEIUhCbf1" role="2Oq$k0">
                                <ref role="3cqZAo" node="5RvEIUhCbf4" resolve="it" />
                              </node>
                              <node concept="1mIQ4w" id="5RvEIUhCbf2" role="2OqNvi">
                                <node concept="chp4Y" id="5RvEIUhCbf3" role="cj9EA">
                                  <ref role="cht4Q" to="ldi9:6FocJsxuw5o" resolve="ActorCompatibility" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="Rh6nW" id="5RvEIUhCbf4" role="1bW2Oz">
                          <property role="TrG5h" value="it" />
                          <node concept="2jxLKc" id="5RvEIUhCbf5" role="1tU5fm" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="5RvEIUhC3Oe" role="3cqZAp">
          <node concept="37vLTw" id="5RvEIUhC3OM" role="3cqZAk">
            <ref role="3cqZAo" node="5RvEIUhC3M1" resolve="desired" />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="5RvEIUhC1rH" role="1B3o_S" />
      <node concept="10P_77" id="5RvEIUhC2A$" role="3clF45" />
      <node concept="P$JXv" id="7nT1Vzx1iwQ" role="lGtFl">
        <node concept="TZ5HA" id="7nT1Vzx1iwR" role="TZ5H$">
          <node concept="1dT_AC" id="7nT1Vzx1iwS" role="1dT_Ay">
            <property role="1dT_AB" value="Return true if the user wish to know the movie genre trend." />
          </node>
        </node>
        <node concept="TZ5HA" id="7nT1Vzx1jF9" role="TZ5H$">
          <node concept="1dT_AC" id="7nT1Vzx1jFa" role="1dT_Ay">
            <property role="1dT_AB" value="" />
          </node>
        </node>
        <node concept="x79VA" id="7nT1Vzx1jTT" role="3nqlJM">
          <property role="x79VB" value="desired : true if the user would like too." />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="6FocJsxvETI" role="jymVt" />
    <node concept="2tJIrI" id="7nT1Vzx1k9X" role="jymVt" />
    <node concept="3clFb_" id="2RDssu5VrPZ" role="jymVt">
      <property role="TrG5h" value="actorPopularity" />
      <node concept="17QB3L" id="1dwac31aZCz" role="3clF45" />
      <node concept="3Tm6S6" id="79RuHsZ$En$" role="1B3o_S" />
      <node concept="3clFbS" id="2RDssu5VrQ2" role="3clF47">
        <node concept="3cpWs8" id="5TFqJhIgvd5" role="3cqZAp">
          <node concept="3cpWsn" id="5TFqJhIgvd8" role="3cpWs9">
            <property role="TrG5h" value="param" />
            <node concept="17QB3L" id="5TFqJhIgvd4" role="1tU5fm" />
            <node concept="Xl_RD" id="5TFqJhIgvec" role="33vP2m">
              <property role="Xl_RC" value="stringParam" />
              <node concept="17Uvod" id="5TFqJhIgvvF" role="lGtFl">
                <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1070475926800/1070475926801" />
                <property role="2qtEX9" value="value" />
                <node concept="3zFVjK" id="5TFqJhIgvvG" role="3zH0cK">
                  <node concept="3clFbS" id="5TFqJhIgvvH" role="2VODD2">
                    <node concept="3clFbF" id="5TFqJhIgvCC" role="3cqZAp">
                      <node concept="2OqwBi" id="2rI3nbr0CHQ" role="3clFbG">
                        <node concept="1PxgMI" id="2rI3nbr0BX3" role="2Oq$k0">
                          <property role="1BlNFB" value="true" />
                          <node concept="chp4Y" id="2rI3nbr0C6_" role="3oSUPX">
                            <ref role="cht4Q" to="ldi9:1yXE__uULMg" resolve="ActorPopularity" />
                          </node>
                          <node concept="30H73N" id="2rI3nbr0z8$" role="1m5AlR" />
                        </node>
                        <node concept="3TrEf2" id="2rI3nbr0D4V" role="2OqNvi">
                          <ref role="3Tt5mk" to="ldi9:1yXE__uV417" resolve="stringParam" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="1dwac31b0sj" role="3cqZAp">
          <node concept="37vLTw" id="56cTEbFdrHH" role="3cqZAk">
            <ref role="3cqZAo" node="5TFqJhIgvd8" resolve="param" />
          </node>
        </node>
      </node>
      <node concept="1WS0z7" id="5TFqJhIgrAk" role="lGtFl">
        <ref role="2rW$FS" node="i474uTf" resolve="ActorPopularityMethod" />
        <node concept="3JmXsc" id="5TFqJhIgrAm" role="3Jn$fo">
          <node concept="3clFbS" id="5TFqJhIgrAo" role="2VODD2">
            <node concept="3clFbF" id="5TFqJhIgrVW" role="3cqZAp">
              <node concept="2OqwBi" id="3GDlSEIPctL" role="3clFbG">
                <node concept="2OqwBi" id="5TFqJhIgs7T" role="2Oq$k0">
                  <node concept="30H73N" id="5TFqJhIgrVV" role="2Oq$k0" />
                  <node concept="3Tsc0h" id="3GDlSEIOWs1" role="2OqNvi">
                    <ref role="3TtcxE" to="ldi9:1yXE__uVKXM" resolve="commands" />
                  </node>
                </node>
                <node concept="3zZkjj" id="3GDlSEIPdVa" role="2OqNvi">
                  <node concept="1bVj0M" id="3GDlSEIPdVc" role="23t8la">
                    <node concept="3clFbS" id="3GDlSEIPdVd" role="1bW5cS">
                      <node concept="3clFbF" id="3GDlSEIPez_" role="3cqZAp">
                        <node concept="2OqwBi" id="3GDlSEIPeUI" role="3clFbG">
                          <node concept="37vLTw" id="3GDlSEIPez$" role="2Oq$k0">
                            <ref role="3cqZAo" node="3GDlSEIPdVe" resolve="it" />
                          </node>
                          <node concept="1mIQ4w" id="3GDlSEIPfpv" role="2OqNvi">
                            <node concept="chp4Y" id="3GDlSEIPkib" role="cj9EA">
                              <ref role="cht4Q" to="ldi9:1yXE__uULMg" resolve="ActorPopularity" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="Rh6nW" id="3GDlSEIPdVe" role="1bW2Oz">
                      <property role="TrG5h" value="it" />
                      <node concept="2jxLKc" id="3GDlSEIPdVf" role="1tU5fm" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="17Uvod" id="5TFqJhIguHb" role="lGtFl">
        <property role="P4ACc" value="ceab5195-25ea-4f22-9b92-103b95ca8c0c/1169194658468/1169194664001" />
        <property role="2qtEX9" value="name" />
        <node concept="3zFVjK" id="5TFqJhIgv3H" role="3zH0cK">
          <node concept="3clFbS" id="5TFqJhIgv3I" role="2VODD2">
            <node concept="3clFbF" id="5TFqJhIgv3J" role="3cqZAp">
              <node concept="2OqwBi" id="5TFqJhIgv3K" role="3clFbG">
                <node concept="1iwH7S" id="5TFqJhIgv3L" role="2Oq$k0" />
                <node concept="2piZGk" id="5TFqJhIgv3M" role="2OqNvi">
                  <node concept="Xl_RD" id="5TFqJhIgv3N" role="2piZGb">
                    <property role="Xl_RC" value="actorPopularity" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="P$JXv" id="7nT1Vzx1lq9" role="lGtFl">
        <node concept="TZ5HA" id="7nT1Vzx1lqa" role="TZ5H$">
          <node concept="1dT_AC" id="7nT1Vzx1lqb" role="1dT_Ay">
            <property role="1dT_AB" value="Internal method : get actor popularity param." />
          </node>
        </node>
        <node concept="TZ5HA" id="7nT1Vzx1_5M" role="TZ5H$">
          <node concept="1dT_AC" id="7nT1Vzx1_5N" role="1dT_Ay">
            <property role="1dT_AB" value="" />
          </node>
        </node>
        <node concept="x79VA" id="7nT1Vzx1_j_" role="3nqlJM">
          <property role="x79VB" value="param : the parameter of an actor popularity DSL method." />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="4pglr71ltK9" role="jymVt" />
    <node concept="3clFb_" id="4pglr71lurg" role="jymVt">
      <property role="TrG5h" value="economicRatio" />
      <node concept="10Q1$e" id="4pglr71lCDj" role="3clF45">
        <node concept="17QB3L" id="4pglr71lurh" role="10Q1$1" />
      </node>
      <node concept="3Tm6S6" id="4pglr71luri" role="1B3o_S" />
      <node concept="3clFbS" id="4pglr71lurj" role="3clF47">
        <node concept="3cpWs8" id="4pglr71lurk" role="3cqZAp">
          <node concept="3cpWsn" id="4pglr71lurl" role="3cpWs9">
            <property role="TrG5h" value="params" />
            <node concept="10Q1$e" id="4pglr71lDqz" role="1tU5fm">
              <node concept="17QB3L" id="4pglr71lurm" role="10Q1$1" />
            </node>
            <node concept="2BsdOp" id="4pglr71lGlp" role="33vP2m">
              <node concept="Xl_RD" id="4pglr71lH1w" role="2BsfMF">
                <property role="Xl_RC" value="sp1" />
                <node concept="17Uvod" id="4pglr71lH1x" role="lGtFl">
                  <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1070475926800/1070475926801" />
                  <property role="2qtEX9" value="value" />
                  <node concept="3zFVjK" id="4pglr71lH1y" role="3zH0cK">
                    <node concept="3clFbS" id="4pglr71lH1z" role="2VODD2">
                      <node concept="3clFbF" id="4pglr71lH1$" role="3cqZAp">
                        <node concept="2OqwBi" id="4pglr71lQut" role="3clFbG">
                          <node concept="1PxgMI" id="4pglr71lQ0o" role="2Oq$k0">
                            <property role="1BlNFB" value="true" />
                            <node concept="chp4Y" id="4pglr71lQab" role="3oSUPX">
                              <ref role="cht4Q" to="ldi9:4pglr71k$m5" resolve="EconomicRatio" />
                            </node>
                            <node concept="30H73N" id="4pglr71lH1C" role="1m5AlR" />
                          </node>
                          <node concept="3TrEf2" id="4pglr71lQQZ" role="2OqNvi">
                            <ref role="3Tt5mk" to="ldi9:4pglr71k$mx" resolve="sp1" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="Xl_RD" id="4pglr71lRZg" role="2BsfMF">
                <property role="Xl_RC" value="sp2" />
                <node concept="17Uvod" id="4pglr71lRZh" role="lGtFl">
                  <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1070475926800/1070475926801" />
                  <property role="2qtEX9" value="value" />
                  <node concept="3zFVjK" id="4pglr71lRZi" role="3zH0cK">
                    <node concept="3clFbS" id="4pglr71lRZj" role="2VODD2">
                      <node concept="3clFbF" id="4pglr71lRZk" role="3cqZAp">
                        <node concept="2OqwBi" id="4pglr71lRZl" role="3clFbG">
                          <node concept="1PxgMI" id="4pglr71lRZm" role="2Oq$k0">
                            <property role="1BlNFB" value="true" />
                            <node concept="chp4Y" id="4pglr71lRZn" role="3oSUPX">
                              <ref role="cht4Q" to="ldi9:4pglr71k$m5" resolve="EconomicRatio" />
                            </node>
                            <node concept="30H73N" id="4pglr71lRZo" role="1m5AlR" />
                          </node>
                          <node concept="3TrEf2" id="4pglr71lUVy" role="2OqNvi">
                            <ref role="3Tt5mk" to="ldi9:4pglr71kUtR" resolve="sp2" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="Xl_RD" id="4pglr71lWpF" role="2BsfMF">
                <property role="Xl_RC" value="sp3" />
                <node concept="17Uvod" id="4pglr71lWpG" role="lGtFl">
                  <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1070475926800/1070475926801" />
                  <property role="2qtEX9" value="value" />
                  <node concept="3zFVjK" id="4pglr71lWpH" role="3zH0cK">
                    <node concept="3clFbS" id="4pglr71lWpI" role="2VODD2">
                      <node concept="3clFbF" id="4pglr71lWpJ" role="3cqZAp">
                        <node concept="2OqwBi" id="4pglr71lWpK" role="3clFbG">
                          <node concept="1PxgMI" id="4pglr71lWpL" role="2Oq$k0">
                            <property role="1BlNFB" value="true" />
                            <node concept="chp4Y" id="4pglr71lWpM" role="3oSUPX">
                              <ref role="cht4Q" to="ldi9:4pglr71k$m5" resolve="EconomicRatio" />
                            </node>
                            <node concept="30H73N" id="4pglr71lWpN" role="1m5AlR" />
                          </node>
                          <node concept="3TrEf2" id="4pglr71m3er" role="2OqNvi">
                            <ref role="3Tt5mk" to="ldi9:4pglr71kUuk" resolve="sp3" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="Xl_RD" id="4pglr71lY9f" role="2BsfMF">
                <property role="Xl_RC" value="sp4" />
                <node concept="17Uvod" id="4pglr71lY9g" role="lGtFl">
                  <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1070475926800/1070475926801" />
                  <property role="2qtEX9" value="value" />
                  <node concept="3zFVjK" id="4pglr71lY9h" role="3zH0cK">
                    <node concept="3clFbS" id="4pglr71lY9i" role="2VODD2">
                      <node concept="3clFbF" id="4pglr71lY9j" role="3cqZAp">
                        <node concept="2OqwBi" id="4pglr71lY9k" role="3clFbG">
                          <node concept="1PxgMI" id="4pglr71lY9l" role="2Oq$k0">
                            <property role="1BlNFB" value="true" />
                            <node concept="chp4Y" id="4pglr71lY9m" role="3oSUPX">
                              <ref role="cht4Q" to="ldi9:4pglr71k$m5" resolve="EconomicRatio" />
                            </node>
                            <node concept="30H73N" id="4pglr71lY9n" role="1m5AlR" />
                          </node>
                          <node concept="3TrEf2" id="4pglr71m5A6" role="2OqNvi">
                            <ref role="3Tt5mk" to="ldi9:4pglr71kUur" resolve="sp4" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="Xl_RD" id="4pglr71m00r" role="2BsfMF">
                <property role="Xl_RC" value="sp5" />
                <node concept="17Uvod" id="4pglr71m00s" role="lGtFl">
                  <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1070475926800/1070475926801" />
                  <property role="2qtEX9" value="value" />
                  <node concept="3zFVjK" id="4pglr71m00t" role="3zH0cK">
                    <node concept="3clFbS" id="4pglr71m00u" role="2VODD2">
                      <node concept="3clFbF" id="4pglr71m00v" role="3cqZAp">
                        <node concept="2OqwBi" id="4pglr71m00w" role="3clFbG">
                          <node concept="1PxgMI" id="4pglr71m00x" role="2Oq$k0">
                            <property role="1BlNFB" value="true" />
                            <node concept="chp4Y" id="4pglr71m00y" role="3oSUPX">
                              <ref role="cht4Q" to="ldi9:4pglr71k$m5" resolve="EconomicRatio" />
                            </node>
                            <node concept="30H73N" id="4pglr71m00z" role="1m5AlR" />
                          </node>
                          <node concept="3TrEf2" id="4pglr71m8p6" role="2OqNvi">
                            <ref role="3Tt5mk" to="ldi9:4pglr71kUu$" resolve="sp5" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="4pglr71lurx" role="3cqZAp">
          <node concept="37vLTw" id="4pglr71lury" role="3cqZAk">
            <ref role="3cqZAo" node="4pglr71lurl" resolve="params" />
          </node>
        </node>
      </node>
      <node concept="1WS0z7" id="4pglr71lurz" role="lGtFl">
        <ref role="2rW$FS" node="diSKvcrG4R" resolve="EconomicRatioMethod" />
        <node concept="3JmXsc" id="4pglr71lur$" role="3Jn$fo">
          <node concept="3clFbS" id="4pglr71lur_" role="2VODD2">
            <node concept="3clFbF" id="4pglr71lurA" role="3cqZAp">
              <node concept="2OqwBi" id="4pglr71lurB" role="3clFbG">
                <node concept="2OqwBi" id="4pglr71lurC" role="2Oq$k0">
                  <node concept="30H73N" id="4pglr71lurD" role="2Oq$k0" />
                  <node concept="3Tsc0h" id="4pglr71lurE" role="2OqNvi">
                    <ref role="3TtcxE" to="ldi9:1yXE__uVKXM" resolve="commands" />
                  </node>
                </node>
                <node concept="3zZkjj" id="4pglr71lurF" role="2OqNvi">
                  <node concept="1bVj0M" id="4pglr71lurG" role="23t8la">
                    <node concept="3clFbS" id="4pglr71lurH" role="1bW5cS">
                      <node concept="3clFbF" id="4pglr71lurI" role="3cqZAp">
                        <node concept="2OqwBi" id="4pglr71lurJ" role="3clFbG">
                          <node concept="37vLTw" id="4pglr71lurK" role="2Oq$k0">
                            <ref role="3cqZAo" node="4pglr71lurN" resolve="it" />
                          </node>
                          <node concept="1mIQ4w" id="4pglr71lurL" role="2OqNvi">
                            <node concept="chp4Y" id="4pglr71lwmm" role="cj9EA">
                              <ref role="cht4Q" to="ldi9:4pglr71k$m5" resolve="EconomicRatio" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="Rh6nW" id="4pglr71lurN" role="1bW2Oz">
                      <property role="TrG5h" value="it" />
                      <node concept="2jxLKc" id="4pglr71lurO" role="1tU5fm" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="17Uvod" id="4pglr71lurP" role="lGtFl">
        <property role="P4ACc" value="ceab5195-25ea-4f22-9b92-103b95ca8c0c/1169194658468/1169194664001" />
        <property role="2qtEX9" value="name" />
        <node concept="3zFVjK" id="4pglr71lurQ" role="3zH0cK">
          <node concept="3clFbS" id="4pglr71lurR" role="2VODD2">
            <node concept="3clFbF" id="4pglr71lurS" role="3cqZAp">
              <node concept="2OqwBi" id="4pglr71lurT" role="3clFbG">
                <node concept="1iwH7S" id="4pglr71lurU" role="2Oq$k0" />
                <node concept="2piZGk" id="4pglr71lurV" role="2OqNvi">
                  <node concept="Xl_RD" id="4pglr71lurW" role="2piZGb">
                    <property role="Xl_RC" value="economicRatio" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="P$JXv" id="7nT1Vzx1C5m" role="lGtFl">
        <node concept="TZ5HA" id="7nT1Vzx1C5n" role="TZ5H$">
          <node concept="1dT_AC" id="7nT1Vzx1C5o" role="1dT_Ay">
            <property role="1dT_AB" value="Internal method : get economic ratio params." />
          </node>
        </node>
        <node concept="TZ5HA" id="7nT1Vzx1Dg9" role="TZ5H$">
          <node concept="1dT_AC" id="7nT1Vzx1Dga" role="1dT_Ay">
            <property role="1dT_AB" value="" />
          </node>
        </node>
        <node concept="x79VA" id="7nT1Vzx1Dzu" role="3nqlJM">
          <property role="x79VB" value="params : the parameters of an economic ratio DSL method." />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="6FocJsxv_oe" role="jymVt" />
    <node concept="2tJIrI" id="7nT1Vzx1DTn" role="jymVt" />
    <node concept="3clFb_" id="6FocJsxuLan" role="jymVt">
      <property role="TrG5h" value="actorCompatibility" />
      <ref role="2rW$FS" node="6FocJsxuxe4" resolve="ActorCompatibilityMethod" />
      <node concept="10Q1$e" id="6FocJsxuLao" role="3clF45">
        <node concept="17QB3L" id="6FocJsxuLap" role="10Q1$1" />
      </node>
      <node concept="3Tm6S6" id="6FocJsxuLaq" role="1B3o_S" />
      <node concept="3clFbS" id="6FocJsxuLar" role="3clF47">
        <node concept="3cpWs8" id="6FocJsxuLas" role="3cqZAp">
          <node concept="3cpWsn" id="6FocJsxuLat" role="3cpWs9">
            <property role="TrG5h" value="params" />
            <node concept="10Q1$e" id="6FocJsxuLau" role="1tU5fm">
              <node concept="17QB3L" id="6FocJsxuLav" role="10Q1$1" />
            </node>
            <node concept="2BsdOp" id="6FocJsxuLaw" role="33vP2m">
              <node concept="Xl_RD" id="6FocJsxuLax" role="2BsfMF">
                <property role="Xl_RC" value="sp1" />
                <node concept="17Uvod" id="6FocJsxuLay" role="lGtFl">
                  <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1070475926800/1070475926801" />
                  <property role="2qtEX9" value="value" />
                  <node concept="3zFVjK" id="6FocJsxuLaz" role="3zH0cK">
                    <node concept="3clFbS" id="6FocJsxuLa$" role="2VODD2">
                      <node concept="3clFbF" id="6FocJsxuLa_" role="3cqZAp">
                        <node concept="2OqwBi" id="6FocJsxwhzj" role="3clFbG">
                          <node concept="1PxgMI" id="6FocJsxwh5r" role="2Oq$k0">
                            <property role="1BlNFB" value="true" />
                            <node concept="chp4Y" id="6FocJsxwhfe" role="3oSUPX">
                              <ref role="cht4Q" to="ldi9:6FocJsxuw5o" resolve="ActorCompatibility" />
                            </node>
                            <node concept="30H73N" id="6FocJsxuLaD" role="1m5AlR" />
                          </node>
                          <node concept="3TrEf2" id="6FocJsxwhVC" role="2OqNvi">
                            <ref role="3Tt5mk" to="ldi9:6FocJsxuw6f" resolve="sp1" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="Xl_RD" id="6FocJsxuLaF" role="2BsfMF">
                <property role="Xl_RC" value="sp2" />
                <node concept="17Uvod" id="6FocJsxuLaG" role="lGtFl">
                  <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1070475926800/1070475926801" />
                  <property role="2qtEX9" value="value" />
                  <node concept="3zFVjK" id="6FocJsxuLaH" role="3zH0cK">
                    <node concept="3clFbS" id="6FocJsxuLaI" role="2VODD2">
                      <node concept="3clFbF" id="6FocJsxwa69" role="3cqZAp">
                        <node concept="2OqwBi" id="6FocJsxwb9U" role="3clFbG">
                          <node concept="1PxgMI" id="6FocJsxwaG2" role="2Oq$k0">
                            <property role="1BlNFB" value="true" />
                            <node concept="chp4Y" id="6FocJsxwaPP" role="3oSUPX">
                              <ref role="cht4Q" to="ldi9:6FocJsxuw5o" resolve="ActorCompatibility" />
                            </node>
                            <node concept="30H73N" id="6FocJsxwa68" role="1m5AlR" />
                          </node>
                          <node concept="3TrEf2" id="6FocJsxwc9_" role="2OqNvi">
                            <ref role="3Tt5mk" to="ldi9:6FocJsxuw6H" resolve="sp2" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="6FocJsxuLbj" role="3cqZAp">
          <node concept="37vLTw" id="6FocJsxuLbk" role="3cqZAk">
            <ref role="3cqZAo" node="6FocJsxuLat" resolve="params" />
          </node>
        </node>
      </node>
      <node concept="1WS0z7" id="6FocJsxuLbl" role="lGtFl">
        <ref role="2rW$FS" node="6FocJsxuxe4" resolve="ActorCompatibilityMethod" />
        <node concept="3JmXsc" id="6FocJsxuLbm" role="3Jn$fo">
          <node concept="3clFbS" id="6FocJsxuLbn" role="2VODD2">
            <node concept="3clFbF" id="6FocJsxuLbo" role="3cqZAp">
              <node concept="2OqwBi" id="6FocJsxuLbp" role="3clFbG">
                <node concept="2OqwBi" id="6FocJsxuLbq" role="2Oq$k0">
                  <node concept="30H73N" id="6FocJsxuLbr" role="2Oq$k0" />
                  <node concept="3Tsc0h" id="6FocJsxuLbs" role="2OqNvi">
                    <ref role="3TtcxE" to="ldi9:1yXE__uVKXM" resolve="commands" />
                  </node>
                </node>
                <node concept="3zZkjj" id="6FocJsxuLbt" role="2OqNvi">
                  <node concept="1bVj0M" id="6FocJsxuLbu" role="23t8la">
                    <node concept="3clFbS" id="6FocJsxuLbv" role="1bW5cS">
                      <node concept="3clFbF" id="6FocJsxuLbw" role="3cqZAp">
                        <node concept="2OqwBi" id="6FocJsxuLbx" role="3clFbG">
                          <node concept="37vLTw" id="6FocJsxuLby" role="2Oq$k0">
                            <ref role="3cqZAo" node="6FocJsxuLb_" resolve="it" />
                          </node>
                          <node concept="1mIQ4w" id="6FocJsxuLbz" role="2OqNvi">
                            <node concept="chp4Y" id="6FocJsxuPE_" role="cj9EA">
                              <ref role="cht4Q" to="ldi9:6FocJsxuw5o" resolve="ActorCompatibility" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="Rh6nW" id="6FocJsxuLb_" role="1bW2Oz">
                      <property role="TrG5h" value="it" />
                      <node concept="2jxLKc" id="6FocJsxuLbA" role="1tU5fm" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="17Uvod" id="6FocJsxuLbB" role="lGtFl">
        <property role="P4ACc" value="ceab5195-25ea-4f22-9b92-103b95ca8c0c/1169194658468/1169194664001" />
        <property role="2qtEX9" value="name" />
        <node concept="3zFVjK" id="6FocJsxuLbC" role="3zH0cK">
          <node concept="3clFbS" id="6FocJsxuLbD" role="2VODD2">
            <node concept="3clFbF" id="6FocJsxuLbE" role="3cqZAp">
              <node concept="2OqwBi" id="6FocJsxuLbF" role="3clFbG">
                <node concept="1iwH7S" id="6FocJsxuLbG" role="2Oq$k0" />
                <node concept="2piZGk" id="6FocJsxuLbH" role="2OqNvi">
                  <node concept="Xl_RD" id="6FocJsxuLbI" role="2piZGb">
                    <property role="Xl_RC" value="actorCompatibility" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="P$JXv" id="7nT1Vzx1F9K" role="lGtFl">
        <node concept="TZ5HA" id="7nT1Vzx1F9L" role="TZ5H$">
          <node concept="1dT_AC" id="7nT1Vzx1F9M" role="1dT_Ay">
            <property role="1dT_AB" value="Internal method : get actor compatibility params." />
          </node>
        </node>
        <node concept="TZ5HA" id="7nT1Vzx1Gkz" role="TZ5H$">
          <node concept="1dT_AC" id="7nT1Vzx1Gk$" role="1dT_Ay">
            <property role="1dT_AB" value="" />
          </node>
        </node>
        <node concept="x79VA" id="7nT1Vzx1GzN" role="3nqlJM">
          <property role="x79VB" value="params : the parameters of an actor compatibility DSL method." />
        </node>
      </node>
    </node>
    <node concept="3Tm1VV" id="5_Hu3f0yqgI" role="1B3o_S" />
    <node concept="n94m4" id="5_Hu3f0yqgJ" role="lGtFl">
      <ref role="n9lRv" to="ldi9:5_Hu3f0yfxm" resolve="Analyzer" />
    </node>
  </node>
</model>

