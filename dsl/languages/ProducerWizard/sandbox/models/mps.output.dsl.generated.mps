<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:539a4d8b-7f7d-4978-af46-7c9f91a1f384(mps.output.dsl.generated)">
  <persistence version="9" />
  <languages>
    <use id="5ba71e2a-36a9-45b2-b967-ed4e3e4898dd" name="ProducerWizard" version="0" />
  </languages>
  <imports />
  <registry>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="5ba71e2a-36a9-45b2-b967-ed4e3e4898dd" name="ProducerWizard">
      <concept id="5066643802732774789" name="ProducerWizard.structure.EconomicRatio" flags="ng" index="270x0O">
        <child id="5066643802732774817" name="sp1" index="270x0g" />
        <child id="5066643802732865444" name="sp5" index="270Z8l" />
        <child id="5066643802732865428" name="sp3" index="270Z8_" />
        <child id="5066643802732865435" name="sp4" index="270Z8E" />
        <child id="5066643802732865399" name="sp2" index="270Zb6" />
      </concept>
      <concept id="1343958697267639540" name="ProducerWizard.structure.StringParam" flags="ng" index="2lMou$" />
      <concept id="6443938805505914966" name="ProducerWizard.structure.Analyzer" flags="ng" index="2pTo1_">
        <child id="1782768328330121074" name="commands" index="16kEkF" />
      </concept>
      <concept id="1782768328329862288" name="ProducerWizard.structure.ActorPopularity" flags="ng" index="16lFr9">
        <child id="1782768328329936967" name="stringParam" index="16kuCu" />
      </concept>
      <concept id="7698959599989817688" name="ProducerWizard.structure.ActorCompatibility" flags="ng" index="1bCzeJ">
        <child id="7698959599989817773" name="sp2" index="1bCzdq" />
        <child id="7698959599989817743" name="sp1" index="1bCzdS" />
      </concept>
      <concept id="6764313106955893768" name="ProducerWizard.structure.MoviesGenreTrend" flags="ng" index="3cg5rA" />
    </language>
  </registry>
  <node concept="2pTo1_" id="1yXE__uVMhv">
    <property role="TrG5h" value="TimBurtonAnalyzer" />
    <node concept="16lFr9" id="3GDlSEIOx_z" role="16kEkF">
      <node concept="2lMou$" id="3GDlSEIOx_$" role="16kuCu">
        <property role="TrG5h" value="brad pitt" />
      </node>
    </node>
    <node concept="270x0O" id="4pglr71lhcT" role="16kEkF">
      <node concept="2lMou$" id="4pglr71lhcV" role="270x0g">
        <property role="TrG5h" value="Iron Man" />
      </node>
      <node concept="2lMou$" id="4pglr71lhcX" role="270Zb6">
        <property role="TrG5h" value="Black Panther" />
      </node>
      <node concept="2lMou$" id="4pglr71lhcZ" role="270Z8_">
        <property role="TrG5h" value="Captain America" />
      </node>
      <node concept="2lMou$" id="4pglr71lhd1" role="270Z8E">
        <property role="TrG5h" value="Doctor Strange" />
      </node>
      <node concept="2lMou$" id="4pglr71lhd3" role="270Z8l">
        <property role="TrG5h" value="Infinity War" />
      </node>
    </node>
    <node concept="270x0O" id="4pglr71m$YF" role="16kEkF">
      <node concept="2lMou$" id="4pglr71m$YH" role="270x0g">
        <property role="TrG5h" value="Daredevil" />
      </node>
      <node concept="2lMou$" id="4pglr71m$YJ" role="270Zb6">
        <property role="TrG5h" value="luke cage" />
      </node>
      <node concept="2lMou$" id="4pglr71m$YL" role="270Z8_">
        <property role="TrG5h" value="null" />
      </node>
      <node concept="2lMou$" id="4pglr71m$YN" role="270Z8E">
        <property role="TrG5h" value="null" />
      </node>
      <node concept="2lMou$" id="4pglr71m$YP" role="270Z8l">
        <property role="TrG5h" value="null" />
      </node>
    </node>
    <node concept="1bCzeJ" id="5RvEIUhBFlK" role="16kEkF">
      <node concept="2lMou$" id="5RvEIUhBFlM" role="1bCzdS">
        <property role="TrG5h" value="brad pitt" />
      </node>
      <node concept="2lMou$" id="5RvEIUhBFlO" role="1bCzdq">
        <property role="TrG5h" value="angelina jolie" />
      </node>
    </node>
    <node concept="3cg5rA" id="5RvEIUhCuYj" role="16kEkF" />
  </node>
</model>

