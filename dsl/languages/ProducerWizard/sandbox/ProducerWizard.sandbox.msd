<?xml version="1.0" encoding="UTF-8"?>
<solution name="ProducerWizard.sandbox" uuid="cd0530bf-69c0-4a3d-bffa-695ceaee9671" moduleVersion="0" compileInMPS="true" generatorOutputPath="/Users/jonathan 1/MPSProjects/M1 ICE - PDC /back-end/src/main/java">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
  </models>
  <sourcePath />
  <dependencies>
    <dependency reexport="true">6354ebe7-c22a-4a0f-ac54-50b52ab9b065(JDK)</dependency>
  </dependencies>
  <languageVersions>
    <language slang="l:5ba71e2a-36a9-45b2-b967-ed4e3e4898dd:ProducerWizard" version="0" />
    <language slang="l:ceab5195-25ea-4f22-9b92-103b95ca8c0c:jetbrains.mps.lang.core" version="1" />
  </languageVersions>
  <dependencyVersions>
    <module reference="6354ebe7-c22a-4a0f-ac54-50b52ab9b065(JDK)" version="0" />
    <module reference="cd0530bf-69c0-4a3d-bffa-695ceaee9671(ProducerWizard.sandbox)" version="0" />
  </dependencyVersions>
</solution>

