# Before everything else

In order to generate the DslTranslator class in the right folder, please set 
your own output path. In MPS, right click on the ProducerWizzard.sandbox > 
Module Properties > Generator output path : set our path to the back-end/src/main/java.

After you've write your own producer program, just right click on the 
ProducerWizzard.sandbox > Rebuild Solution.

You can now start the Back-end server.

You can now open the Front-end program. Keep in mind that data may take a while
before being extracted.


# Global Guideline

### Guidelines
Development environment
- IDE Java : IntelliJ
- IDE DSL : MPS
- Shared code repository : GitLab
- Complete CI : GitLab, Sonar

Management platform (User stories, Task board, Panning and tasks management) : GitLab

Communication channels : Slack (m1ice-pdc-team2.slack.com)

#### General Planning
![General planning](docs/gant_general.png)


# Sprint 1 review

### Global Process
![Sprint #1 global process](docs/Global_Process.jpg)

### DSL meta-model
![Sprint #1 meta-model](docs/Meta_model_Sprint1.jpg)

### Planning
![Sprint #1 planning](docs/sprint_1.png)

### Class Diagram of Java Structure
![Java structure](docs/java_structure.png)

### Meta Model Diagram 
![Java structure](docs/meta_modele.png)



# MPS Configuration


### How to generate your DSL program into the Spring Server

Open `m1ice-pdc-team2` in MPS.
- Right click on `ProducerWizard.sandbox` > `Module Properties`.
- In `Generator output path` enter your own path to `m1ice-pdc-team2/basicwebapp/src/main/java`.
- Close the window, right click again on  `ProducerWizard.sandbox` >  `Rebuild solution`.



# Definition of done (DOD)

* Development
* Tests -> several key aspects with well *named* methods
  * 1 test => 1 assert => 1 implicit method name
* Documentation -> objective : have to be clear for the people who do the complete doc
  * Java -> JavaDoc
  * React -> commentaire
