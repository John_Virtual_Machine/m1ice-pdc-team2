import React from 'react';
import { Bar, Line, Pie } from 'react-chartjs-2';
import { PaletteContext } from './PaletteProvider';

const Charts = ({ data, type, contextPalette }) => {
  const palette = i => contextPalette[i % contextPalette.length];
  const datasets = data.datasets.map((el, i) => ({
    ...el,
    backgroundColor:
      type === 'Line' ? '#3F51B5AA' : el.data.map((e, j) => palette(j)),
    borderColor:
      type === 'Line' ? '#3F51B5' : el.data.map((e, j) => palette(j)),
  }));
  const dataWithColor = data;
  dataWithColor.datasets = datasets;
  const chartProps = {
    redraw: true,
    data: dataWithColor,
    options: {
      scales: type !== 'Pie' && {
        yAxes: [
          {
            ticks: {
              beginAtZero: true,
            },
          },
        ],
      },
      responsive: true,
      maintainAspectRatio: false,
      tooltips: {
        mode: 'point',
        intersect: true,
      },
      legend: {
        display: false,
      },
    },
  };
  switch (type) {
    case 'Bar':
      return <Bar {...chartProps} />;
    case 'Line':
      return <Line {...chartProps} />;
    case 'Pie':
      return <Pie {...chartProps} />;
    default:
      return <div>Unknown type</div>;
  }
};

export default props => (
  <PaletteContext.Consumer>
    {context => <Charts {...props} contextPalette={context.state.palette} />}
  </PaletteContext.Consumer>
);
