import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import blueGrey from '@material-ui/core/colors/blueGrey';
import lightBlue from '@material-ui/core/colors/lightBlue';
import red from '@material-ui/core/colors/red';
import PaletteProvider from './PaletteProvider';

const theme = createMuiTheme({
  palette: {
    primary: blueGrey,
    secondary: lightBlue,
    error: red,
    contrastThreshold: 3,
    tonalOffset: 0.2,
  },
});

ReactDOM.render(
  <MuiThemeProvider theme={theme}>
    <PaletteProvider>
      <App />
    </PaletteProvider>
  </MuiThemeProvider>,
  document.getElementById('root'),
);
