import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Charts from './Charts';
import Spinner from './Spinner';
import DATA from './data/DATA';

const styles = {
  mainApp: {
    display: 'grid',
    gridTemplateColumns: '1fr 1fr',
    gridTemplateRows: '1fr 1fr',
    backgroundColor: '#DBDBDB',
  },
  card: {
    margin: 10,
    display: 'grid',
    gridTemplateRow: '70px 1fr',
  },
  spinner: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
};

class MainApp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      DATA: [],
    };
  }

  componentWillMount() {
    fetch('http://localhost:8080/')
       .then(res => res.json())
       .then(res => this.setState({ DATA: res }));
    // this.setState({
    //   DATA,
    // });
  }

  renderCard = card => {
    const dataID = this.state.DATA.find(el => el.id === card.data).id;
    return (
      <Card
        className={this.props.classes.card}
        style={{ gridRow: card.row, gridColumn: card.column }}
      >
        <CardHeader
          title={this.state.DATA[dataID].name}
          subheader={this.state.DATA[dataID].subtitle}
        />
        <CardContent>
          <Charts data={this.state.DATA[dataID].data} type={card.type} />
        </CardContent>
      </Card>
    );
  };

  render() {
    const { classes, cards } = this.props;
    return this.state.DATA.length > 0 ? (
      <div className={classes.mainApp}>{cards.map(this.renderCard)}</div>
    ) : (
      <div className={classes.spinner}>
        <Spinner />
      </div>
    );
  }
}

export default withStyles(styles)(MainApp);
