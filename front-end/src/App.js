import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import './App.css';
import MainApp from './MainApp';
import { PaletteContext } from './PaletteProvider';
import { TextField } from '@material-ui/core';

const styles = {
  app: {
    display: 'grid',
    gridTemplateColumns: '1fr 300px',
    height: 'calc(100% - 64px)',
  },
  drawer: {
    backgroundColor: '#263238',
    color: 'white',
    height: '100%',
  },
  drawerList: {
    colorPrimary: '#ffffff',
  },
  root: {
    height: '100vh',
  },
  title: {
    flexGrow: 1,
  },
  tabs: {
    alignSelf: 'flex-end',
    width: 252,
  },
};
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeScreen: undefined,
      screens: undefined,
      selectedTab: 0,
    };
    fetch('http://localhost:8080/screens')
      .then(res => res.json())
      .then(res =>
        this.setState({
          screens: res,
          activeScreen: res[0],
        }),
      );
  }

  selectScreen = screen => {
    this.setState({
      activeScreen: screen,
    });
  };

  handleChangeTab = (e, tab) => {
    this.setState({
      selectedTab: tab,
    });
  };

  colorWatch = ({ color, changeColor, index }) => (
    <ListItem>
      <ListItemText disableTypography>
        <Typography
          style={{
            color: '#FFFFFF',
            display: 'flex',
            alignItems: 'center',
          }}
        >
          <div
            style={{
              width: 10,
              height: 10,
              backgroundColor: color,
              marginRight: 5,
            }}
          />
          <TextField
            value={color}
            onChange={event => changeColor(index, event.target.value)}
            inputProps={{ style: { color: 'white' } }}
          />
        </Typography>
      </ListItemText>
    </ListItem>
  );

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <AppBar position="static" color="primary">
          <Toolbar>
            <Typography
              className={classes.title}
              variant="title"
              color="inherit"
            >
              Producer Wizzard
            </Typography>
            <Tabs
              className={classes.tabs}
              centered
              value={this.state.selectedTab}
              onChange={this.handleChangeTab}
            >
              <Tab label="Screens" />
              <Tab label="Options" />
            </Tabs>
          </Toolbar>
        </AppBar>
        <div className={classes.app}>
          {this.state.activeScreen && (
            <MainApp cards={this.state.activeScreen.cards} />
          )}
          <div className={classes.drawer}>
            {this.state.selectedTab === 0 && (
              <List component="nav">
                {this.state.screens &&
                  this.state.screens.map(screen => (
                    <ListItem button onClick={() => this.selectScreen(screen)}>
                      <ListItemText disableTypography>
                        <Typography
                          style={{
                            color:
                              this.state.activeScreen === screen
                                ? '#00b0ff'
                                : '#FFFFFF',
                          }}
                        >
                          {screen.name}
                        </Typography>
                      </ListItemText>
                    </ListItem>
                  ))}
              </List>
            )}
            {this.state.selectedTab === 1 && (
              <List component="nav">
                {this.props.contextPalette.map((color, index) => (
                  <this.colorWatch
                    color={color}
                    index={index}
                    changeColor={this.props.changeColor}
                  />
                ))}
              </List>
            )}
          </div>
        </div>
      </div>
    );
  }
}

const AppWithContext = withStyles(styles)(App);

export default props => (
  <PaletteContext.Consumer>
    {context => (
      <AppWithContext
        {...props}
        contextPalette={context.state.palette}
        changeColor={context.changeColor}
      />
    )}
  </PaletteContext.Consumer>
);
