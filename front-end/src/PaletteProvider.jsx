import React, { Component } from 'react';
const PaletteContext = React.createContext();

class PaletteProvider extends Component {
  state = {
    palette: [
      '#3F51B5',
      '#009688',
      '#FFC107',
      '#673AB7',
      '#00BCD4',
      '#795548',
      '#f44336',
      '#4CAF50',
      '#FFC107',
      '#9C27B0',
      '#FFEB3B',
    ],
  };
  render() {
    return (
      <PaletteContext.Provider
        value={{
          state: this.state,
          changePalette: newPalette => this.setState({ palette: newPalette }),
          changeColor: (index, newColor) =>
            this.setState(oldState => {
              const newPalette = oldState.palette;
              newPalette[index] = newColor;
              return { palette: newPalette };
            }),
        }}
      >
        {this.props.children}
      </PaletteContext.Provider>
    );
  }
}

export default PaletteProvider;
export { PaletteContext };
